<?php

namespace app\assets;

/**
 * Application Asset File.
 */
class MemberResourcesAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/resources';

    public $css = [
        '//fonts.googleapis.com/css?family=Stardos+Stencil:400,700',
        '//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css',
        'css/templates/admin/css/bootstrap.min.css',
        'css/templates/admin/css/animate.min.css',
        'css/templates/admin/css/light-bootstrap-dashboard.css',
        'css/templates/admin/css/pe-icon-7-stroke.css',
        'css/fd-icon-fonts.css',
        'css/templates/admin/main.css'
    ];

    public $js = [
        '//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js',
        'css/templates/admin/js/bootstrap.min.js',
        'css/templates/admin/js/bootstrap-notify.js',
        'css/templates/admin/js/light-bootstrap-dashboard.js',
        'css/templates/admin/js/fd-main.js'
    ];

    public $publishOptions = [
        'only' => [
            'css/*',
            'css/templates/admin/*',
            'css/templates/admin/css/*',
            'css/templates/admin/fonts/*',
            'css/templates/admin/js/*',
            'css/templates/admin/img/*'
        ]
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];

}
