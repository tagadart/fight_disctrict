<?php

namespace app\assets;

/**
 * Application Asset File.
 */
class LandingAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/resources';

    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css'
    ];

    public $js = [
    	'//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js',
        'js/grid.js',
        'js/main.js',
        'js/aos.js',
    ];

}
