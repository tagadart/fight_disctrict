<?php

namespace app\assets;

/**
 * Application Asset File.
 */
class ResourcesAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/resources';

    public $css = [
        '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        '//fonts.googleapis.com/css?family=Stardos+Stencil:400,700',
        '//fonts.googleapis.com/css?family=Lato:300,400,700',
        'css/templates/admin/css/pe-icon-7-stroke.css',
        'css/fd-icon-fonts.css',
        'css/main.css'
    ];

    public $js = [
        '//maps.googleapis.com/maps/api/js?key=AIzaSyD4ajt-gXE_jOCY-3DiS3drq8E6tPKKVTc',
        'css/templates/site/lib/jquery/jquery-migrate.min.js',
        'css/templates/site/lib/superfish/hoverIntent.js',
        'css/templates/site/lib/superfish/superfish.min.js',
        'css/templates/site/lib/tether/tether.min.js',
        'css/templates/site/lib/stellar/stellar.min.js',
        'css/templates/site/lib/counterup/counterup.min.js',
        'css/templates/site/lib/waypoints/waypoints.min.js',
        'css/templates/site/lib/easing/easing.js',
        'css/templates/site/lib/stickyjs/sticky.js',
        'css/templates/site/lib/parallax/parallax.js',
        'css/templates/site/lib/lockfixed/lockfixed.min.js',
        'css/templates/site/js/custom.js',
        'js/modernizr.custom.js',
        '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js',
        '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js',
        '//cdn.jsdelivr.net/npm/afterglowplayer@1.1'
    ];

    public $publishOptions = [
        'only' => [
            'css/templates/site/css/*',
            'css/templates/site/js/*',
            'css/templates/site/lib/**/*',
            'css/templates/site/img/*',
            'css/*',
            'css/fonts/*',
            'js/*',
        ],
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];

}
