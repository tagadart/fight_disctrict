<?php

namespace app\assets;

/**
 * Application Asset File.
 */
class PrintAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/resources';

    public $css = [
        '//fonts.googleapis.com/css?family=Stardos+Stencil:400,700',
        '//fonts.googleapis.com/css?family=Lato:300,400,700',
        'css/templates/admin/css/bootstrap.min.css',
        'css/templates/admin/print.css'
    ];

    public $cssOptions = [
        'media' => 'print',
    ];

    public $js = [];

    public $publishOptions = [];

    public $depends = [];

}
