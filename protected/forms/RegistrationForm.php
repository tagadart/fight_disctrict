<?php

namespace app\forms;

use Da\User\Form\RegistrationForm as BaseForm;

class RegistrationForm extends BaseForm 
{
	public $captcha;

    /**
     * Override from parent
     */
    public function rules() 
    {
        $rules = parent::rules();
        $rules['usernameRequired'] = ['username', 'string'];

        $rules[] = [['captcha'], 'required'];
        $rules[] = [['captcha'], 'Da\User\Validator\ReCaptchaValidator'];
        
        return $rules;
    }

}
