<?php

namespace app\forms;

use Yii;
use Da\User\Form\SettingsForm as BaseForm;

class SettingsForm extends BaseForm 
{

	/**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'new_password' => Yii::t('app', 'Nouveau mot de passe'),
            'current_password' => Yii::t('app', 'Mot de passe actuel'),
        ];
    }

    /**
     * Override from parent
     */
    public function rules() 
    {
        $rules = parent::rules();
        $rules['usernameRequired'] = ['username', 'string'];
        $rules['newPasswordRequired'] = ['new_password', 'required'];
        return $rules;
    }

}
