<?php

namespace app\notifications;

use webzop\notifications\Notification;
use Yii;

class ScheduleNotification extends Notification
{
    const KEY_NEW_ACCOUNT = 'test';
    const KEY_RESET_PASSWORD = 'reset_password';

    /**
     * @var \yii\web\User the user object
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        switch ($this->key) {
            case self::KEY_NEW_ACCOUNT:
                return Yii::t('app', 'New account {user} created', ['user' => '#' .' $this->user->id']);
            case self::KEY_RESET_PASSWORD:
                return Yii::t('app', 'Instructions to reset the password');
        }
    }

    /**
     * @inheritdoc
     */
    public function getRoute()
    {
        return ['/users/edit', 'id' => $this->user->id];
    }
}
