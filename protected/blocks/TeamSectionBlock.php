<?php

namespace app\blocks;

use app\modules\teammember\models\TeamMember;
use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;

/**
 * Team Section Block.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4.
 */
class TeamSectionBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Section Team';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'group';
    }

    public function extraVars()
    {
        return [
            'teamMembers' => TeamMember::find()->all(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'title', 'label' => 'Titre', 'type' => self::TYPE_TEXT],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     */
    public function admin()
    {
        return '<p>Section Team</p>';
    }
}
