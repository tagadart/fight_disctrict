<?php

namespace app\blocks;

use app\modules\training\models\Training;
use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Training Section Block.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4.
 */
class TrainingSectionBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Section Cours';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'school';
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'title', 'label' => 'Titre', 'type' => self::TYPE_TEXT],
                ['var' => 'pdf_file', 'label' => 'PDF', 'type' => self::TYPE_FILEUPLOAD],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        $trainings = Training::find()->orderBy(['item_order' => SORT_ASC])->all();

        $days = [
            'Lundi' => 1,
            'Mardi' => 2,
            'Mercredi' => 3,
            'Jeudi' => 4,
            'Vendredi' => 5,
            'Samedi' => 6,
            'Dimanche' => 7,
        ];

        return [
            'trainings' => $trainings,
            'weekDays' => $days,
            'pdf_file' => BlockHelper::fileUpload($this->getVarValue('pdf_file'), true),
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.title}}
     */
    public function admin()
    {
        return '<p>Section Cours</p>';
    }
}
