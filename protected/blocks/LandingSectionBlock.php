<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Landing Section Block.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4.
 */
class LandingSectionBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Section Landing';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'insert_photo';
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'title', 'label' => 'Titre', 'type' => self::TYPE_TEXT],
                ['var' => 'subtitle', 'label' => 'Sous-titre', 'type' => self::TYPE_TEXT],
                ['var' => 'image', 'label' => 'Image', 'type' => self::TYPE_FILEUPLOAD],
                ['var' => 'logo', 'label' => 'Logo', 'type' => self::TYPE_FILEUPLOAD],
            ],
            'cfgs' => [
                ['var' => 'image_slider', 'label' => 'Carousel', 'type' => self::TYPE_CHECKBOX],
            ],
            'placeholders' => [
                ['var' => 'carousel', 'label' => 'Carousel'],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        return [
            'image' => BlockHelper::fileUpload($this->getVarValue('image'), true),
            'logo' => BlockHelper::fileUpload($this->getVarValue('logo'), true),
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{extras.image}}
     * @param {{vars.image}}
     * @param {{vars.subtitle}}
     * @param {{vars.title}}
     */
    public function admin()
    {
        return '<p>Section Landing</p>';
    }
}
