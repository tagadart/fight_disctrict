<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;

/**
 * Access Block.
 *
 * File has been created with `block/create` command.
 */
class AccessBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Accès';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'directions';
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'accessLine', 'label' => 'Ligne', 'type' => self::TYPE_TEXT],
                ['var' => 'url', 'label' => 'Lien', 'type' => self::TYPE_LINK],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.accessLine}}
     * @param {{vars.title}}
     * @param {{vars.url}}
     */
    public function admin()
    {
        return '<p>{{vars.accessLine}}</p>';
    }
}
