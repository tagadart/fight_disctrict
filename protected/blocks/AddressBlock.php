<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;

/**
 * Address Block.
 *
 * File has been created with `block/create` command.
 */
class AddressBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Adresse';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'location_searching';
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'contactDetail', 'label' => 'Contact', 'type' => self::TYPE_LIST_ARRAY],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.contactDetail}}
     */
    public function admin()
    {
        return '<p>Infos de contact</p>';
    }
}
