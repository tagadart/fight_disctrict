<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;

/**
 * Schedule Block.
 *
 * File has been created with `block/create` command.
 */
class ScheduleBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Horaires';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'schedule';
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'info', 'label' => 'Infos', 'type' => self::TYPE_LIST_ARRAY],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.info}}
     */
    public function admin()
    {
        return '<p>Infos horaires</p>';
    }
}
