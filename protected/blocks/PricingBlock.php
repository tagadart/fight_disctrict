<?php

namespace app\blocks;

use app\modules\pricing\models\Pricing;
use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;

/**
 * Pricing Block.
 *
 * File has been created with `block/create` command.
 */
class PricingBlock extends PhpBlock
{

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Section Tarifs';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'attach_money';
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'title', 'label' => 'Titre', 'type' => self::TYPE_TEXT],
                ['var' => 'description', 'label' => 'Description', 'type' => self::TYPE_TEXTAREA],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function extraVars()
    {
        return [
            'pricings' => Pricing::find()->all(),
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.description}}
     * @param {{vars.title}}
     */
    public function admin()
    {
        return '<p>Section Tarifs</p>';
    }
}
