<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;

/**
 * Contact Section Block.
 *
 * File has been created with `block/create` command.
 */
class ContactSectionBlock extends PhpBlock
{
    /**
     * @var boolean Choose whether block is a layout/container/segmnet/section block or not, Container elements will be optically displayed
     * in a different way for a better user experience. Container block will not display isDirty colorizing.
     */
    public $isContainer = true;

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Section Contact';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'contact_mail';
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'cfgs' => [
                ['var' => 'contact_title', 'label' => 'Titre', 'type' => self::TYPE_TEXT],
                ['var' => 'access_title', 'label' => 'Titre Accès', 'type' => self::TYPE_TEXT],
                ['var' => 'schedules_title', 'label' => 'Titre Horaire', 'type' => self::TYPE_TEXT],
            ],
            'vars' => [
                ['var' => 'title', 'label' => 'Titre', 'type' => self::TYPE_TEXT],
                ['var' => 'description', 'label' => 'Description', 'type' => self::TYPE_TEXTAREA],
            ],
            'placeholders' => [
                ['var' => 'contactForm', 'label' => 'Formulaire de contact'],
                ['var' => 'address', 'label' => 'Adresse'],
                ['var' => 'access', 'label' => 'Accès'],
                ['var' => 'schedules', 'label' => 'Horaires'],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{cfgs.description}}
     * @param {{cfgs.title}}
     * @param {{vars.description}}
     * @param {{vars.title}}
     */
    public function admin()
    {
        return '<p>Section Contact</p>';
    }

}
