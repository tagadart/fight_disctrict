<?php

namespace app\controllers;

use app\forms\RegistrationForm;
use Da\User\Event\FormEvent;
use Da\User\Event\UserEvent;
use Da\User\Validator\AjaxRequestModelValidator;
use Da\User\Model\User;
use Da\User\Factory\MailFactory;
use Da\User\Service\UserRegisterService;
use Da\User\Service\AccountConfirmationService;
use Da\User\Service\UserConfirmationService;

use app\modules\member\models\Member;

use Yii;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\filters\AccessControl;
use Carbon\Carbon;

use Da\User\Controller\RegistrationController as BaseController;

class RegistrationController extends BaseController 
{

    public $enableCsrfValidation = false;

	public function init()
    {
        parent::init();
        $this->layout = '@app/views/layouts/login-register';
    }

     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['register', 'connect'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['confirm', 'resend'],
                        'roles' => ['?', '@'],
                    ],
                ]
            ],
        ];
    }

    /**
     * [actionRegister description]
     * @return [type] [description]
     */
	public function actionRegister()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        /** @var RegistrationForm $form */
        $form = new RegistrationForm();
        $member = new Member();

        /** @var FormEvent $event */
        $event = $this->make(FormEvent::class, [$form]);

        $member->attributes = Yii::$app->request->post('Member');
        $form->attributes = Yii::$app->request->post('RegistrationForm');

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($member, $form);
        }

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $this->trigger(FormEvent::EVENT_BEFORE_REGISTER, $event);

            /** @var User $user */
            $user = $this->make(User::class, [], $form->attributes);
            $user->setScenario('register');

            $mailService = MailFactory::makeWelcomeMailerService($user);

            if ($form->validate() && $member->validate()) {

                if ($this->make(UserRegisterService::class, [$user, $mailService])->run()) {

                    $member->user_id = $user->id;
                    $member->date_of_birth = Carbon::createFromFormat('d.m.Y', $member->attributes['date_of_birth'])->timestamp;
                    $member->save();

                    Yii::$app->session->setFlash('register-success', Yii::t('app', 'Votre compte a été créé et un message avec des instructions supplémentaires a été envoyé à votre adresse e-mail'));

                    $this->trigger(FormEvent::EVENT_AFTER_REGISTER, $event);
                    return $this->refresh();
                }
                Yii::$app->session->setFlash('danger', Yii::t('app', 'L\'utilisateur n\'a pas pu être enregistré.'));
            }
            
        }

        return $this->render('register', ['model' => $form, 'member' => $member, 'module' => $this->module]);
    }

    /**
     * [actionConfirm description]
     * @param  [type] $id   [description]
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function actionConfirm($id, $code)
    {
        /** @var User $user */
        $user = $this->userQuery->whereId($id)->one();

        if ($user === null || $this->module->enableEmailConfirmation === false) {
            throw new NotFoundHttpException();
        }

        /** @var UserEvent $event */
        $event = $this->make(UserEvent::class, [$user]);
        $userConfirmationService = $this->make(UserConfirmationService::class, [$user]);

        $this->trigger(UserEvent::EVENT_BEFORE_CONFIRMATION, $event);

        if ($this->make(AccountConfirmationService::class, [$code, $user, $userConfirmationService])->run()) {
            Yii::$app->user->login($user, $this->module->rememberLoginLifespan);
            Yii::$app->session->setFlash('validation-success', Yii::t('usuario', 'Bienvenue sur Fight-District, votre inscription est maintenant terminée.'));
            $this->trigger(UserEvent::EVENT_AFTER_CONFIRMATION, $event);
        } else {
            Yii::$app->session->setFlash('validation-error', Yii::t('usuario', 'The confirmation link is invalid or expired. Please try requesting a new one.'));
        }

        return $this->redirect(['/member/index']);
    }

}