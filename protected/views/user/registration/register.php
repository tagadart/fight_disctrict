<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use Da\User\Widget\ReCaptchaWidget;

use app\modules\pricing\models\Pricing;

/**
 * @var yii\web\View                   $this
 * @var \Da\User\Form\RegistrationForm $model
 * @var \Da\User\Model\User            $user
 * @var \Da\User\Module                $module
 */

$this->title = Yii::t('app', 'Fight-District | Enregistrement');

?>

<div id="user-section" class="container">
    <section class="register-form">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Yii::t('app', 'Inscription') ?></h3>
                    </div>

                    <?php if ($flash = Yii::$app->session->getFlash('register-success')): ?>
                        <div class="alert alert-success">
                            <p><?= $flash ?></p>
                        </div>
                    <?php else: ?>

                    <div class="panel-body">
                        <?php $form = ActiveForm::begin(
                            [
                                'id' => $model->formName(),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false
                            ]
                        ); ?>
                        <div class="row form-group">
                            <?= $form->field($member, 'gender', ['options' => [ 'class' => 'col-md-12 gender-radio']])
                                ->radioList([
                                    1 => Yii::t('app', 'Mme.'),
                                    2 => Yii::t('app', 'M.')
                                ])->label(false);
                            ?>
                        </div>

                        <div class="row form-group">
                            <?= $form->field($member, 'first_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Prénom'], 'options' => ['class' => 'col-md-6']])->label(false) ?>
                            <?= $form->field($member, 'last_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Nom'], 'options' => ['class' => 'col-md-6']])->label(false) ?>
                        </div>

                        <?= $form->field($member, 'date_of_birth', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Date de naissance (ex. 21.02.1970)']])->label(false)
                            //->widget(\yii\widgets\MaskedInput::className(), ['name' => 'date_of_birth', 'clientOptions' => ['alias' =>  'mm.dd.yyyy']]) ?>

                        <?= $form->field($member, 'member_type')
                            ->dropDownList(ArrayHelper::map(Pricing::find()->asArray()->all(), 'id', 'category'), ['autofocus' => true])
                            ->label(false)
                        ?>

                        <?= $form->field($model, 'email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Email']])->label(false) ?>

                        <?php if ($module->generatePasswords == false): ?>
                            <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Mot de passe']])->label(false)->passwordInput() ?>
                        <?php endif ?>

                        <?= $form->field($model, 'captcha')->widget(ReCaptchaWidget::className())->label(false) ?>

                        <div class="row btn-container">
                            <?= Html::submitButton(Yii::t('app', 'Créer mon compte'), ['class' => 'btn btn-block']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>

                    <?php endif; ?>
                </div>
                <p class="text-center">
                    <?= Html::a(Yii::t('app', 'Vous avez déjà un compte? Connectez-vous!'), ['/user/security/login']) ?>
                </p>
            </div>
        </div>
    </section>
</div>
