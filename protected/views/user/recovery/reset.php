<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View               $this
 * @var yii\widgets\ActiveForm     $form
 * @var \Da\User\Form\RecoveryForm $model
 */

$this->title = Yii::t('app', 'Fight-District | Reinitialisation du mot de passe');

?>

<div id="user-section" class="container">
    <section class="pwd-reset-form">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Yii::t('app', 'Reinitialisation du mot de passe') ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php $form = ActiveForm::begin(
                            [
                                'id' => $model->formName(),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                            ]
                        ); ?>

                        <?= $form->field($model, 'password')->passwordInput()->label('Mot de passe') ?>
                        
                        <div class="row btn-container">
                        <?= Html::submitButton(Yii::t('app', 'Reinitialiser'), ['class' => 'btn btn-block']) ?><br>
                        </div>
                        
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
