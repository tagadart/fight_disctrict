<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View               $this
 * @var yii\widgets\ActiveForm     $form
 * @var \Da\User\Form\RecoveryForm $model
 */

$this->title = Yii::t('app', 'Fight-District | Récupération du mot de passe');

?>

<div id="user-section" class="container">
    <section class="pwd-recover-form">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Yii::t('app', 'Récupération du mot de passe') ?></h3>
                    </div>
                     <?php if ($flash = Yii::$app->session->getFlash('request-success')): ?>
                        <div class="alert alert-success">
                            <p><?= $flash ?></p>
                        </div>
                    <?php else: ?>
                    <div class="panel-body">
                        <?php $form = ActiveForm::begin(
                            [
                                'id' => $model->formName(),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                            ]
                        ); ?>
                        <?= $form->field($model, 'email')->label('Email')->textInput(['autofocus' => true]) ?>

                        <div class="row btn-container">
                        <?= Html::submitButton(Yii::t('usuario', 'Envoyer'), ['class' => 'btn btn-block']) ?><br>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div> 
    </section>
</div>
