<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Da\User\Widget\ConnectWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View            $this
 * @var \Da\User\Form\LoginForm $model
 * @var \Da\User\Module         $module
 */

$this->title = Yii::t('app', 'Fight-District | Connexion');

?>

<div id="user-section" class="container">
	<section class="login-form">
		<div class="row">
		    <div class="col-md-12">
		    	<?php if ($flash = Yii::$app->session->getFlash('pwd-changed-success')): ?>
                    <div class="alert alert-success">
                        <p><?= $flash ?></p>
                    </div>
                <?php endif; ?>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h3 class="panel-title"><?= Yii::t('app', 'Connexion') ?></h3>
		            </div>
		            <div class="panel-body">
		                <?php $form = ActiveForm::begin(
		                    [
		                        'id' => $model->formName(),
		                        'enableAjaxValidation' => true
		                   	])
		                ?>
		                <?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1', 'placeholder' => 'Email']])->label(false)
		                ?>
		                <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2', 'placeholder' => 'Mot de passe']])
		                	->passwordInput()
		                    ->label(($module->allowPasswordRecovery ? Html::a(Yii::t('app', 'Mot de passe oublié?'), ['/user/recovery/request'], ['tabindex' => '5']) : ''))
		                ?>
		                <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4', 'label' => 'Se souvenir de moi']) ?>
		                <div class="row btn-container">
		                <?= Html::submitButton(Yii::t('app', 'Connexion'), ['class' => 'btn btn-block', 'tabindex' => '3']) ?>
		            	</div>
		                <?php ActiveForm::end(); ?>
		            </div>
		        </div>
		        <?php if ($module->enableRegistration): ?>
		            <p class="text-center">
		                <?= Html::a(Yii::t('app', 'Pas encore de compte? Créer mon compte!'), ['/user/registration/register']) ?>
		            </p>
		        <?php endif ?>
		    </div>
		</div>
	</section>
</div>
