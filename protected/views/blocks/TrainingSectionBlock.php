<?php

use yii\helpers\ArrayHelper;

$formatter = \Yii::$app->formatter;

/**
 * View file for block: TrainingSectionBlock.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4.
 *
 * @param $this->varValue('title');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>
<h2 class="text-center"><?= $this->varValue('title'); ?></h2>
<div class="row" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
	<div class="col-md-12">
	    <ul class="gallery-items">
			<?php foreach ($this->extraValue('trainings') as $key => $training): ?>
	        <li class="gallery-item">
	            <div class="gallery-contents">
	                <div class="thumbnail gallery-trigger">
	                    <div class="feature-col">
	                        <div class="card card-block text-center">
	                            <div>
	                                <div class="feature-icon">
	                                    <span class="<?= $training->icon; ?>"></span>
	                                </div>
	                            </div>
	                            <div>
	                                <h3><?= $training->title; ?></h3>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="gallery-expander">
	                <div class="gallery-expander-contents">
	                    <div class="gallery-trigger-close close"><span class="pe-7s-close"></span></div>
	                    <div class="col col-4 icon-section">
	                    	<span class="<?= $training->icon; ?>"></span>
							<?php if (isset($training->video->source) || !empty($training->video_url)): ?>
							<div class="video-container">
		                    	<a class="afterglow" href="#video_<?= $key ?>"><span class="pe-7s-play"></span></a>
		                    	<?php if ($training->is_youtube): ?>
		                    		<video id="video_<?= $key ?>" width="1280" height="720" data-youtube-id="<?= $training->video_url ?>"></video>
							    <?php else: ?>
									<video id="video_<?= $key ?>" width="1280" height="720">
								    	<source type="video/mp4" src="<?= $training->video->source ?>" />
								    </video>
							    <?php endif; ?>

						    </div>
							<?php endif; ?>
	                    </div>
	                    <div class="col col-md-8 col-xs-12">
	                        <div class="title"><?= $training->title; ?></div>
							<div class="contents"><?= $training->description; ?></div>
							<div class="schedule-section">
								<table class="table">
									<tr>
										<th>Jour</th>
										<th class="text-center">Horaires</th>
										<th class="text-center">Places restantes</th>
									</tr>
									<?php foreach ($training->schedules as $schedule): ?>
										<tr>
											<td><?= $schedule->translatedDay ?></td>
											<td class="text-center"><?= $formatter->asDate($schedule->start_time, 'HH:mm') . ' - ' . $formatter->asDate($schedule->end_time, 'HH:mm') ?></td>
											<?php if ($schedule->count > 0): ?>
                                            <td class="text-center"><?= $schedule->count ?></td>
                                            <?php else: ?>
                                            <td class="text-center">Complet</td>
                                            <?php endif ?>
										</tr>
									<?php endforeach; ?>
									
									<!--
									<tr style="text-align: center;">
										<th></th>
										<?php foreach ($training->associatedArray[1] as $key => $day): ?>
										<th><?= $day ?></th>
										<?php endforeach; ?>
									</tr>

									<?php foreach ($training->associatedArray[0] as $time => $schedules): ?>
										<tr>
											<td style="font-weight: bold"><?= $formatter->asDate($schedules[0]->start_time, 'HH:mm') ?> - <?= $formatter->asDate($schedules[0]->end_time, 'HH:mm') ?></td>
											
											<?php foreach ($training->associatedArray[1] as $key => $day): ?>
												<td style="text-align: center;">
												<?php foreach ($schedules as $skey => $schedule): ?>
													<?php if($key == $schedule->training_day): ?>
													<span class="pe-7s-check"></span>
													<?php endif ?>
												<?php endforeach; ?>
												</td>
											<?php endforeach; ?>
										</tr>
									<?php endforeach; ?>
									-->
								</table>
								<div class="btn-container">
									<a href="#pricing">Voir les tarifs</a>
									<a href="<?= $this->extraValue('pdf_file')->getSource() ?>" target="_blank">Horaire hebdomadaire</a>
								</div>
							</div>
							
	                    </div>
	                </div>
	            </div>
	        </li>
	    	<?php endforeach; ?>
	    </ul>
	</div>
</div>