<?php
/**
 * View file for block: ContactSectionBlock 
 *
 * File has been created with `block/create` command. 
 *
 * @param $this->varValue('description');
 * @param $this->varValue('title');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>

<div class="row wrapper" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
    <div class="col-md-6 form-section">
        <h2 class="section-title"><?= $this->varValue('title') ?></h2>
        <div class="form">
            <?= $this->placeholderValue('contactForm') ?>
        </div>
    </div>
    <div class="col-md-6 detail-section">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <address>
                    <strong><?= $this->cfgValue('contact_title') ?></strong>
                    <?= $this->placeholderValue('address') ?>
                </address>
            </div>
            <div class="col-md-4 col-sm-12">
                <address>
                    <strong><?= $this->cfgValue('access_title') ?></strong>
                    <?= $this->placeholderValue('access') ?>
                </address>
            </div>
            <div class="col-md-4 col-sm-12">
                <address>
                    <strong><?= $this->cfgValue('schedules_title') ?></strong>
                    <?= $this->placeholderValue('schedules') ?>
                </address>
            </div>
        </div>
        <div class="row map">
            <div id="map"></div>
        </div>
    </div>
</div>

<div class="row copyright">
    <div>© <?= date('Y') ?> Fight-District Sàrl - Tous droits réservés | Design <a href="http://tagadart.com" target="_target">Tagadart</a></div>
</div>
