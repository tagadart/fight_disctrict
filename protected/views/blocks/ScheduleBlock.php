<?php
/**
 * View file for block: ScheduleBlock 
 *
 * File has been created with `block/create` command. 
 *
 * @param $this->varValue('info');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>

<?php foreach ($this->varValue('info') as $info): ?>
<p><?= $info['value'] ?></p>
<?php endforeach; ?>