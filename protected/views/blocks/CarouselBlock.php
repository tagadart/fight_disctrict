<?php
/**
 * View file for block: CarouselBlock 
 *
 * File has been created with `block/create` command. 
 *
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>


<div class="slider">
    <?php foreach($this->extraValue('images') as $image): ?>
    <div class="img-container" style="background-image: url(<?= $image['source'] ?>)"></div>
    <?php endforeach;?>
</div>
