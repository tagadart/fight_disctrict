<?php
/**
 * View file for block: AccessBlock 
 *
 * File has been created with `block/create` command. 
 *
 * @param $this->varValue('accessLine');
 * @param $this->varValue('url');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>

<p><a href="<?= $this->varValue('url')['value'] ?>" target="_blank"><?= $this->varValue('accessLine') ?></a></p>