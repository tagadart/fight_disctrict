<?php

use yii\helpers\Html;

/**
 * View file for block: PricingBlock 
 *
 * File has been created with `block/create` command. 
 *
 * @param $this->varValue('description');
 * @param $this->varValue('title');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>
<h2 class="text-center"><?= $this->varValue('title') ?></h2>
<div class="row" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
    <div class="col-md-12">
        <div class="cd-pricing-container cd-has-margins">
            <ul class="cd-pricing-list">
            	<?php foreach($this->extraValue('pricings') as $pricing): ?>
        		<li>
                    <header class="cd-pricing-header">
                        <h2><?= $pricing->category ?></h2>
                        <div class="cd-price">
                            <span class="cd-currency"><?= $pricing->currency ?></span>
                            <span class="cd-value"><?= $pricing->amount ?></span>
                            <span class="cd-duration"><?= $pricing->duration ?></span>
                        </div>
                    </header>

                    <div class="cd-pricing-body">
                        <ul class="cd-pricing-features">
                            <?php foreach($pricing->periods as $index => $period): ?>

                                <li class="red-text"><?= $period->description ?></li>
                                
                                <?php for($i = 0; $i <= $period->row_config - 1 ; $i++): ?>
                                    <?php if (!empty($period->rates[$i]->description) && isset($period->rates[$i]->frequency) && isset($period->rates[$i]->amount)): ?>
                                        <li><?= $period->rates[$i]->description . ' ' . $period->rates[$i]->frequency . ' x '?>CHF <?= $period->rates[$i]->amount ?>.-</li>
                                    <?php else: ?>
                                        <li>-</li>
                                    <?php endif; ?>
                                <?php endfor; ?>
                                
                                <?php if ($index != (count($pricing->periods) - 1) ): ?>
                                <li class="empty-row"></li>
                                <?php endif; ?>

                            <?php endforeach; ?>

                        </ul>
                    </div>

                    <footer class="cd-pricing-footer">
                        <?= Html::a('S\'inscrire', ['/user/register'], ['class' => 'cd-select']); ?>
                    </footer>
                </li>
            	<?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>