<?php
/**
 * View file for block: AddressBlock 
 *
 * File has been created with `block/create` command. 
 *
 * @param $this->varValue('contactDetail');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>
<?php foreach ($this->varValue('contactDetail') as $info): ?>
<p><?= $info['value'] ?></p>
<?php endforeach; ?>