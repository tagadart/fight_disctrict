<?php
/**
 * View file for block: LandingSectionBlock 
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4. 
 *
 * @param $this->extraValue('image');
 * @param $this->varValue('image');
 * @param $this->varValue('subtitle');
 * @param $this->varValue('title');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>

<?php if ($this->cfgValue('image_slider')): ?>
<section class="slider-container">
	<?php if ($this->extraValue('logo')): ?>
	<div class="text-center" data-aos="zoom-in" data-aos-duration="1000" data-aos-once="true">
		<img style="width: 40%;" src="<?= $this->extraValue('logo')->getSource() ?>">
	</div>
	<?php endif; ?>
	<?= $this->placeholderValue('carousel') ?>
</section>

<?php else: ?>
<section class="hero" style="background-image: url(<?= $this->extraValue('image')->getSource() ?>)">
	<div class="container text-center" data-aos="zoom-in" data-aos-duration="1000" data-aos-once="true">
		<div class="row">
			<div class="col-md-12">
				<?php if ($this->extraValue('logo')): ?>
				<img style="width: 40%;" src="<?= $this->extraValue('logo')->getSource() ?>">
				<?php endif; ?>
			</div>
		</div>
		<div class="col-md-12">
			<?php if ($this->varValue('title')): ?>
			<h1><?= $this->varValue('title') ?></h1>
			<?php endif; ?>
			<?php if ($this->varValue('subtitle')): ?>
			<p class="tagline"><?= $this->varValue('subtitle') ?></p>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>