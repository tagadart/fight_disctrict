<?php
/**
 * View file for block: TeamSectionBlock
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4.
 *
 * @param $this->varValue('title');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>
<h2 class="text-center"><?= $this->varValue('title') ?></h2>
<div class="row" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
	<?php foreach($this->extraValue('teamMembers') as $teamMember): ?>
    <div class="col-sm-3 col-xs-6">
        <div class="card card-block">
        	<img alt="" class="team-img" src="<?= $teamMember->image->source ?>">
        	<div class="card-title-wrap">
            	<span class="card-title"><?= $teamMember->first_name ?> <?= $teamMember->last_name ?></span>
                <div class="trainings">
                    <?php if(is_array($teamMember->training)): ?>
                    <?php foreach($teamMember->training as $training): ?>
                        <span><?= $training ?></span>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>
          	</div>
          	<div class="team-over">
          		<span class="card-text"><?= $teamMember->function ?></span>
                <p><?= $teamMember->description ?></p>
                <div class="prize-list">
                    <?php if(is_array($teamMember->training)): ?>
                        <?php foreach($teamMember->prize_list as $prize): ?>
                        <span><?= $prize ?></span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
        		<nav class="social-nav">
        			<?php if (!empty($teamMember->facebook_link)): ?>
	            	<a href="<?= $teamMember->facebook_link ?>" target="_blank"><i class="fa fa-facebook"></i></a>
	            	<?php endif; ?>

	            	<?php if (!empty($teamMember->instagram_link)): ?>
	            	<a href="<?= $teamMember->instagram_link ?>" target="_blank"><i class="fa fa-instagram"></i></a>
	            	<?php endif; ?>

	            	<?php if (!empty($teamMember->youtube_link)): ?>
	            	<a href="<?= $teamMember->youtube_link ?>" target="_blank"><i class="fa fa-youtube"></i></a>
	            	<?php endif; ?>

	            	<?php if (!empty($teamMember->email)): ?>
	            	<a href="mailto:<?= $teamMember->email ?>"><i class="fa fa-envelope"></i></a>
	            	<?php endif; ?>
            	</nav>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>