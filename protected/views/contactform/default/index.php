<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var object $model Contains the model object based on DynamicModel yii class. */
/* @var $this \luya\web\View */
/* @var $form \yii\widgets\ActiveForm */

?>

<?php if (Yii::$app->session->getFlash('contactform_success')): ?>
    <div class="alert alert-success">Nous vous remercions pour votre message et y répondrons dès que possible !</div>
<?php else: ?>

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'contactForm'
        ]
    ]); ?>
    
    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Prénom | Nom'])->label(false) ?>
    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
    <?= $form->field($model, 'subject')->textInput(['placeholder' => 'Sujet'])->label(false) ?>
    <?= $form->field($model, 'message')->textarea(['placeholder' => 'Message', 'rows' => 4])->label(false) ?>
    
    <div class="text-right">
    <?= Html::submitButton('Envoyer', ['class' => 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
<?php endif; ?>