<?php

use app\assets\ResourcesAsset;
use yii\helpers\Html;

ResourcesAsset::register($this);

use app\modules\settings\models\Settings;

$settings = Settings::find(['image_url'])->one();

?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->composition->language; ?>">

    <head>
        <meta charset="UTF-8" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?= Html::csrfMetaTags() ?>
        <title><?= $this->title; ?></title>
        <?php $this->head(); ?>
    </head>

    <body>
    <?php $this->beginBody(); ?>
        
        <div id="login-register" class="container-fluid">
            <div class="row">
                <div class="col-md-7 left-side" style="background-image: url(<?= $settings->image_url->source ?>)">
                    <div id="nav-menu-container">
                        <nav class="menu-items">
                            <ul class="nav-menu">
                                <li><a href="../#features">Cours</a></li>
                                <li><a href="../#pricing">Tarifs</a></li>
                                <li><a href="../#team">Team</a></li>
                                <li><a href="../#contact">Infos pratiques</a></li>
                                <li><a href="https://fightdistrict.many-ways.ch/" target="_blank">Shop</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-5 right-side">
                    <?= $content; ?>
                </div>
            </div>
        </div>

    <?php $this->endBody(); ?>
    </body>
    
</html>
<?php $this->endPage(); ?>
