<?php
use app\assets\ResourcesAsset;
use yii\helpers\Html;

ResourcesAsset::register($this);

/* @var $this luya\web\View */
/* @var $content string */

?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->composition->language; ?>">

    <head>
        <meta charset="UTF-8" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?= Html::csrfMetaTags() ?>
        <title><?= $this->title; ?></title>
        <?php $this->head(); ?>
    </head>

    <body>
    <?php $this->beginBody(); ?>
        <div id="fd-main">
        <?= $content; ?>
        </div>
    <?php $this->endBody(); ?>
    </body>
    
</html>
<?php $this->endPage(); ?>
