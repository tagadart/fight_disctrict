<?php

use yii\helpers\Html;
use app\assets\LandingAsset;

LandingAsset::register($this);

?>

<!-- /Hero -->
<?= $placeholders['landingSection']; ?>

<!-- Header -->
<header id="header">
    <div class="container">
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li><a href="#features">Cours</a></li>
                <li><a href="#pricing">Tarifs</a></li>
                <li><a href="#team">Team</a></li>
                <li><a href="#contact">Infos pratiques</a></li>
                <li><a href="https://fightdistrict.many-ways.ch/" target="_blank">Shop</a></li>
            </ul>
        </nav>
        <!-- #nav-menu-container -->
        <nav class="nav social-nav pull-right d-none d-lg-inline">
            <?php if (Yii::$app->user->getIsGuest()): ?>
            <?= Html::a('Connexion', ['user/login']); ?>
            <?= Html::a('Inscription', ['user/register']); ?>
            <?php else: ?>
            <?= Html::a('Mon compte', ['member/default/index']); ?>
            <?= Html::a('Déconnexion', ['/user/security/logout'], ['data-method' => 'post']); ?>
            <?php endif; ?>
        </nav>
    </div>
</header>
<!-- #header -->

<!-- Training -->
<section class="features" id="features">
    <div class="container">
        <?= $placeholders['trainingSection']; ?>
    </div>
</section>
<!-- /Training -->

<!-- Pricing -->
<section class="pricing" id="pricing">
    <div class="container">
        <?= $placeholders['pricingSection']; ?>
    </div>
</section>
<!-- /Pricing -->

<!-- Team -->
<section class="team" id="team">
    <div class="container">
        <?= $placeholders['teamSection']; ?>
    </div>
</section>
<!-- /Team -->

<!-- Contact -->
<section class="contact" id="contact">
    <div class="container">
        <?= $placeholders['contactSection']; ?>
    </div>
</section>
<!-- /Contact -->

<a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a>
