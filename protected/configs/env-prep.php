<?php

/**
 * This config should be used on preproduction enviroment.
 * The preproduction enviroment will be used to show the website to the customer and prepare it for prod deployment.
 */

/*
 * Enable or disable the debugging, if those values are deleted YII_DEBUG is false and YII_ENV is prod.
 * The YII_ENV value will also be used to load assets based on enviroment (see assets/ResourcesAsset.php)
 */
defined('YII_ENV') or define('YII_ENV', 'prep');
defined('YII_DEBUG') or define('YII_DEBUG', false);

return [
	/*
     * For best interoperability it is recommended that you use only alphanumeric characters when specifying an application ID
     */
    'id' => 'fightdistrict',

    /*
     * Application name
     */
    'name' => 'Fight-District',

    /*
     * The name of your site, will be display on the login screen
     */
    'siteTitle' => 'Fight-District',

    /*
     * Let the application know which module should be executed by default (if no url is set). This module must be included
     * in the modules section. In the most cases you are using the cms as default handler for your website. But the concept
     * of LUYA is also that you can use the Website without the CMS module!
     */
    'defaultRoute' => 'cms',

    /*
     * Define the basePath of the project (Yii Configration Setup)
     */
    'basePath' => dirname(__DIR__),

    'modules' => [

       /**
         * Member module
         */
        'memberfrontend' => [
            'class' => 'app\modules\member\frontend\Module',
            'useAppViewPath' => false,
            'urlRules' => [
                ['pattern' => 'member/<action>', 'route' => 'member/default/<action>']
            ]
        ],
        'member' => 'app\modules\member\frontend\Module',
        'memberadmin' => 'app\modules\member\admin\Module',

        /**
         * Training module
         */
        'trainingfrontend' => [
            'class' => 'app\modules\training\frontend\Module',
            'useAppViewPath' => true,
        ],
        'trainingadmin' => 'app\modules\training\admin\Module',

        /**
         * Team member module
         */
        'teammemberfrontend' => [
            'class' => 'app\modules\teammember\frontend\Module',
            'useAppViewPath' => true,
        ],
        'teammemberadmin' => 'app\modules\teammember\admin\Module',

        /**
         * Schedule module
         */
        'schedulefrontend' => [
            'class' => 'app\modules\schedule\frontend\Module',
            'useAppViewPath' => true,
        ],
        'scheduleadmin' => 'app\modules\schedule\admin\Module',

        /**
         * Pricing module
         */
        'pricingfrontend' => [
            'class' => 'app\modules\pricing\frontend\Module',
            'useAppViewPath' => true,
        ],
        'pricingadmin' => 'app\modules\pricing\admin\Module',

         /**
         * Application settings
         */
        'settingsfrontend' => [
            'class' => 'app\modules\settings\frontend\Module',
            'useAppViewPath' => true,
        ],
        'settingsadmin' => 'app\modules\settings\admin\Module',

        /*
         * If you have other admin modules (like cmsadmin) then you going to need the admin. The Admin module provides
         * a lot of functionality, like storage, user, permission, crud, etc. But the basic concept of LUYA is also that you can use LUYA without the
         * admin module.
         *
         * @secureLogin: (boolean) This will activate a two-way authentification method where u get a token sent by mail, for this feature
         * you have to make sure the mail component is configured correctly. You can test this with console command `./vendor/bin/luya health/mailer`.
         */
        'admin' => [
            'class' => 'luya\admin\Module',
            'secureLogin' => false, // when enabling secure login, the mail component must be proper configured otherwise the auth token mail will not send.
            'interfaceLanguage' => 'fr', // Admin interface default language. Currently supported: "en", "de", "fr", "es", "ru", "it", "ua", "el".
        ],

        /*
         * Frontend module for the `cms` module.
         */
        'cms' => [
            'class' => 'luya\cms\frontend\Module',
            'contentCompression' => true, // compressing the cms output (removing white spaces and newlines)
        ],

        /*
         * Admin module for the `cms` module.
         */
        'cmsadmin' => [
            'class' => 'luya\cms\admin\Module',
            'hiddenBlocks' => [
                'app\blocks\AddressBlock',
                'app\blocks\ScheduleBlock',
                'luya\cms\frontend\blocks\HtmlBlock',
            ],
            'blockVariations' => [],
        ],

        'user' => [
            'class' => Da\User\Module::class,
            'controllerMap' => [
                'security' => 'app\controllers\SecurityController',
                'registration' => 'app\controllers\RegistrationController',
                'recovery' => 'app\controllers\RecoveryController'
            ],
            'classMap' => [
                'RegistrationForm' => 'app\forms\RegistrationForm',
                'SettingsForm' => 'app\forms\SettingsForm'
            ],
            'mailParams' => [
                'fromEmail' => ['info@fight-district.ch' => 'Fight-District'],
                'welcomeMailSubject' => 'Bienvenu sur Fight-District',
                'confirmationMailSubject' => 'Fight-District | Confirmation de votre compte',
                'reconfirmationMailSubject' => 'Fight-District | Confirmation de changement d\'email',
                'recoveryMailSubject' => 'Fight-District | Demande de réinitialisation du mot de passe',
            ],
            'rememberLoginLifespan' => '172800'
        ],

        'contactform' => [
            'class' => 'luya\contactform\Module',
            'useAppViewPath' => true,
            'mailTitle' => 'Formulaire de contact',
            'attributes' => [
                'name', 'email', 'subject', 'message',
            ],
            'attributeLabels' => [
                'name' => 'Nom | Prénom',
                'email' => 'Email',
                'subject' => 'Sujet',
                'message' => 'Message',
            ],
            'rules' => [
                [['name', 'email', 'subject', 'message'], 'required'],
                ['email', 'email'],
            ],
            'recipients' => [
                'hello@jdl-stack.it',
            ],
        ],

    ],
    'components' => [

        /*
        'request' => [
            'cookieValidationKey' => 'me9M5KPGpji_WjU3HZGBbWQLTYa9oEZR'
        ],
        */
       
       'recaptcha' => [
            'class' => 'Da\User\Component\ReCaptchaComponent',
            'key' => '6Ld4L0sUAAAAAEfwBvrtf_7v7xoSK6pUKfxvfAdf',
            'secret' => '6Ld4L0sUAAAAAHegOCn3bBeynRYrAORLAo6OqueN'
        ],

        'response' => [
            'formatters' => [
                'pdf' => [
                    'class' => 'robregonm\pdf\PdfResponseFormatter',
                    'mode' => '',
                    'format' => 'A4',
                    'defaultFontSize' => 0,
                    'defaultFont' => '',
                    'marginLeft' => 15,
                    'marginRight' => 15,
                    'marginTop' => 16,
                    'marginBottom' => 16,
                    'marginHeader' => 9,
                    'marginFooter' => 9,
                    'orientation' => 'Landscape',
                    'options' => [
                        'tempDir' => '@app/runtime/tmp'
                    ]
                ],
            ]
        ],

        'stripe' => [
            'class' => 'ruskid\stripe\Stripe',
            'publicKey' => 'pk_test_K8HNgByiWKhzovUzpfEVoIuL',
            'privateKey' => 'sk_test_1d5KRJHOfubjxlBIxHhEU1dq',
        ],

    	'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=lgen.myd.infomaniak.com;dbname=lgen_fight_district_2018',
            'username' => 'lgen_jdl-stack',
            'password' => 'Deltalima747*',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 43200
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@Da/User/resources/views' => '@app/views/user'
                ]
            ]
        ],

        'storage' => [
            'class' => 'luya\admin\filesystem\LocalFileSystem',
            'serverPath' => realpath(dirname(__FILE__).'/../../public_html/storage'),
            'whitelistMimeTypes' => ['text/plain']
        ],

        'formatter' => [
            'datetimeFormat' => [
                'fr' => 'dd.MM.yyyy HH:mm:ss',
            ],
            'dateFormat' => 'dd.MM.yyyy',
            'timeFormat' => 'HH:mm',
            'currencyCode' => 'CHF',
            'decimalSeparator' => '.'
        ],

        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => true
        ],

        /*
         * Add your smtp connection to the mail component to send mails (which is required for secure login), you can test your
         * mail component with the luya console command ./vendor/bin/luya health/mailer.
         */
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.infomaniak.com',
                'username' => 'hello@jdl-stack.it',
                'password' => 'Adminjdls747*',
                'port' => '587'
            ]
        ],

        'mail' => [
            'host' => 'mail.infomaniak.com',
            'username' => 'hello@jdl-stack.it',
            'password' => 'Adminjdls747*',
            'from' => 'hello@jdl-stack.it',
            'fromName' => 'Fight-District'
        ],

        /*
         * ATTENTION:
         * To help us improve our Software you can enable (true) this property to send all Exceptions directly to the luya developer team.
         * The follwing informations will be transfered:
         * - $_GET, $_POST, $_SERVER and $_SESSION data
         * - Exception Object (inlcuding stack trace, line, linenr, message, file)
         *
         * You can also create your own errorapi (https://github.com/luyadev/luya-module-errorapi) module to get notification
         * about the errors from your projects.
         */
        'errorHandler' => [
            'transferException' => true,
            'class' => 'luya\web\ErrorHandler',
            'api' => 'https://jdl-stack.it/errorapi'
        ],

        /*
         * The composition component handles your languages and they way your urls will look like. The composition componentn will
         * automatically add the language prefix you have defined in `default` to your url (the language part in the url "example.com/EN/homepage").
         *
         * hidden: (boolean) If this website is not multilingual you can hidde the composition, other whise you have to enable this.
         * default: (array) Contains the default setup for the current language, this must match your language system configuration.
         */
        'composition' => [
            'hidden' => true, // you will not have languages in your url (most case for pages which are not multi lingual)
            'default' => ['langShortCode' => 'fr'], // the default language for the composition should match your default language shortCode in the langauge table.
        ],

        /*
         * When you are enabling the cache, luya will cache cms blocks and speed up the system in different ways. In the prep config
         * we use the DummyCache to "fake" the caching behavior, but actually nothing gets cached, when your in production you should
         * use caching which matches your hosting environment. In most cases yii\caching\FileCache will result in fast website.
         *
         * http://www.yiiframework.com/doc-2.0/guide-caching-data.html#cache-apis
         */
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],

        /*
    	 * Translation component. If you don't have translations just remove this component and the folder `messages`.
    	 */
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'usuario' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ]
            ]
        ]
    ]
];
