jQuery(document).ready(function() {

	jQuery('.training-btn').click(function(evt) {
		
		$.ajax({
		    url: evt.target.href,
		    type: 'GET',
		    success: function(response) {
		    	$.confirm({
		    		columnClass: 'lg',
		    		theme: 'dark',
				    title: 'Conditions générales de vente',
				    content: response.settings,
				    closeAnimation: 'none',
				    buttons: {
				    	'J\'accepte': function() {
				    		location.href = response.link;
				    	},
				    	'Je n\'accepte pas': function() {}
				    }
				});
		    	return false;
		    }
		});

		return false;
	});

	jQuery('.alert-tmp').delay(4000).fadeOut('2000');

});