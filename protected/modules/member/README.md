# Member Module
 
File has been created with `module/create` command on LUYA version 1.0.0-RC4. 
 
## Installation

In order to add the modules to your project go into the modules section of your config:

```php
return [
    'modules' => [
        // ...
        'member' => 'app\modules\member\frontend\Module',
        'memberadmin' => 'app\modules\member\admin\Module',
        // ...
    ],
];
```