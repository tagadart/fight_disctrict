<?php

namespace app\modules\member\frontend\traits;

use Yii;

use app\modules\member\models\MemberSchedule;
use app\modules\member\models\MemberPricingRate;
use app\modules\training\models\Training;
use app\modules\pricing\models\Invoice;

use Carbon\Carbon;

trait MemberTrait
{

    public function getTraining($status)
    {
        return Training::find()
            ->orderBy(['item_order' => SORT_ASC])
            ->andWhere(['is_special' => $status])
            ->all();
    }
    
    public function initSchedulesArray($trainings, $day = null)
    {
        $schedulesArray = [];

        foreach ($trainings as $trainingKey => $training) {

            $schedules = isset($day) ? $training->getSchedulesByDay($day) : $training->schedules;

            foreach ($schedules as $scheduleKey => $schedule) {

                $model = MemberSchedule::find()
                    ->andWhere(['jdls_member_detail_id' => $this->member->id])
                    ->andWhere(['jdls_schedule_id' => $schedule->id])
                    ->one();

                if (!isset($model) && $model->isNewRecord) {
                    $newModel = new MemberSchedule();
                    $newModel->jdls_member_detail_id = $this->member->id;
                    $newModel->jdls_schedule_id = $schedule->id;
                    $newModel->is_subscribed = 0;
                    $newModel->save();
                    $schedulesArray[$trainingKey][$scheduleKey] = $newModel;
                } else {
                    $schedulesArray[$trainingKey][$scheduleKey] = $model;
                }
            }

        }
        return $schedulesArray;
    }

    public function getMemberPricingModel($status)
    {
    	return MemberPricingRate::find()
            ->joinWith(['rate'])
            ->andFilterWhere(['jdls_pricing_rate.is_special' => $status])
            ->andWhere(['jdls_member_detail_id' => $this->member->id])
            ->one();
    }

    public function getInvoices($status)
    {
        return Invoice::find()
            ->andWhere(['jdls_member_id' => $this->member->id])
            ->andWhere(['is_special' => $status])
            ->orderBy(['status' => SORT_DESC])
            ->orderBy(['start_date' => SORT_ASC])->all();
    }

    public function getCurrentDay()
    {
        $now = Carbon::now('Europe/Zurich');
        return $now->dayOfWeek;
    }

    public function getDays()
    {
        return [1 => 'Lundi', 2 => 'Mardi', 3 => 'Mercredi', 4 => 'Jeudi', 5 => 'Vendredi', 6 => 'Samedi', 7 => 'Dimanche'];
    }

}
