<?php

namespace app\modules\member\frontend\controllers;

use app\modules\member\frontend\traits\MemberTrait;
use app\modules\member\models\Address;
use app\modules\member\models\Member;
use app\modules\member\models\MemberPricingRate;
use app\modules\pricing\models\Invoice;
use app\modules\pricing\models\Pricing;
use app\modules\pricing\models\PricingRate;
use app\modules\settings\models\Settings;
use app\modules\training\models\Training;
use Carbon\Carbon;
use Da\User\Event\UserEvent;
use Da\User\Filter\AccessRuleFilter;
use Da\User\Form\SettingsForm;
use Da\User\Model\User;
use Da\User\Traits\ContainerAwareTrait;
use Da\User\Validator\AjaxRequestModelValidator;
use kartik\mpdf\Pdf;
use luya\web\Controller;
use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

class DefaultController extends Controller
{

    use ContainerAwareTrait, MemberTrait;

    /**
     * @var UserQuery
     */
    protected $member;

    protected $memberType;

    protected $settings;

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['POST'],
                    'validate' => ['GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRuleFilter::class,
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'training', 'training-weekly', 'subscription', 'invoice', 'settings', 'validate', 'payment', 'view-invoice', 'logout', 'send-notification'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/login']);
        }

        $this->member = Member::find()
            ->where(['user_id' => Yii::$app->user->id])->one();

        $this->memberType = $this->member->member_type;

        $this->settings = Settings::find()->one();
        $this->member->date_of_birth = Yii::$app->formatter->asDate($this->member->date_of_birth);

        Yii::$app->view->params['days'] = $this->getDays();
    }

    /**
     * Personal information update action
     * @return [type] [description]
     */
    public function actionIndex()
    {
        $address = Address::find()
            ->where(['jdls_member_id' => $this->member->id])->one();

        if (!isset($address)) {
            $address = new Address();
        }

        $this->member->attributes = Yii::$app->request->post('Member');
        $address->attributes = Yii::$app->request->post('Address');

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($this->member, $address);
        }

        if (Yii::$app->request->post('Member') && $this->member->validate()) {

            $this->member->date_of_birth = Carbon::createFromFormat('d.m.Y', $this->member->date_of_birth)->timestamp;
            $this->member->member_type = $this->memberType;
            $this->member->update();
            Yii::$app->session->setFlash('saved-success', Yii::t('app', 'Enregistrement effectué'));
            return $this->refresh();
        }

        if (Yii::$app->request->post('Address') && $address->validate()) {
            if ($address->isNewRecord) {
                $address->save();
            } else {
                $address->update();
            }

            Yii::$app->session->setFlash('saved-success', Yii::t('app', 'Enregistrement effectué'));
            return $this->refresh();
        }

        return $this->render('index', ['member' => $this->member, 'address' => $address]);

    }

    /**
     * Training POST action
     * @return [type] [description]
     */
    public function actionTraining()
    {

        $trainings = $this->getTraining(false);
        $specialTrainings = $this->getTraining(true);

        $memberPricing = $this->getMemberPricingModel(false);
        $memberSpecialPricing = $this->getMemberPricingModel(true);

        $schedulesArray = $this->initSchedulesArray($trainings);
        $specialSchedulesArray = $this->initSchedulesArray($specialTrainings);

        if (Yii::$app->request->post('MemberSchedule')) {
            if ((bool) Yii::$app->request->get('q')) {
                foreach ($specialTrainings as $trainingKey => $training) {
                    foreach ($training->schedules as $scheduleKey => $schedule) {
                        $postData = Yii::$app->request->post('MemberSchedule')[$trainingKey][$scheduleKey];
                        $currentModel = $specialSchedulesArray[$trainingKey][$scheduleKey];
                        $currentModel->is_subscribed = (int) $postData['is_subscribed'];
                        $currentModel->is_special_training = true;
                        $currentModel->update();
                    }
                }
                Yii::$app->session->setFlash('training-saved-success', Yii::t('app', 'Enregistrement effectué'));
                return $this->refresh();
            } else {
                foreach ($trainings as $trainingKey => $training) {
                    foreach ($training->schedules as $scheduleKey => $schedule) {
                        $postData = Yii::$app->request->post('MemberSchedule')[$trainingKey][$scheduleKey];
                        $currentModel = $schedulesArray[$trainingKey][$scheduleKey];
                        $currentModel->is_subscribed = (int) $postData['is_subscribed'];
                        $currentModel->update();
                    }
                }
                Yii::$app->session->setFlash('training-saved-success', Yii::t('app', 'Enregistrement effectué'));
                return $this->refresh();
            }
        }

        return $this->render('training', [
            'trainings' => $trainings,
            'schedulesArray' => $schedulesArray,
            'specialTrainings' => $specialTrainings,
            'specialSchedulesArray' => $specialSchedulesArray,
            'memberPricing' => $memberPricing,
            'memberSpecialPricing' => $memberSpecialPricing,
            'member' => $this->member,
        ]);

    }

    /**
     * [actionTrainingWeekly description]
     * @param  [type] $day [description]
     * @return [type]      [description]
     */
    public function actionTrainingWeekly($day = null)
    {

        if (isset($day)) {
            $currentDay = $day;
        } else {
            $rawDate = date('m.d.y');
            $currentDay = date('N', strtotime($rawDate));
        }

        $this->view->params['currentDay'] = $currentDay;

        $trainings = $this->getTraining(false);
        $specialTrainings = $this->getTraining(true);

        $schedulesArray = $this->initSchedulesArray($trainings, $currentDay);
        $specialSchedulesArray = $this->initSchedulesArray($specialTrainings, $currentDay);

        $memberPricing = $this->getMemberPricingModel(false);
        $memberSpecialPricing = $this->getMemberPricingModel(true);

        if (Yii::$app->request->post('MemberSchedule')) {
            if ((bool) Yii::$app->request->get('q')) {
                foreach ($specialTrainings as $trainingKey => $training) {
                    foreach ($training->schedules as $scheduleKey => $schedule) {
                        $postData = Yii::$app->request->post('MemberSchedule')[$trainingKey][$scheduleKey];
                        $currentModel = $specialSchedulesArray[$trainingKey][$scheduleKey];
                        $currentModel->is_subscribed = (int) $postData['is_subscribed'];
                        $currentModel->is_special_training = true;
                        $currentModel->update();
                    }
                }
                Yii::$app->session->setFlash('training-saved-success', Yii::t('app', 'Enregistrement effectué'));
                return $this->refresh();
            } else {
                foreach ($trainings as $trainingKey => $training) {
                    foreach ($training->schedules as $scheduleKey => $schedule) {
                        $postData = Yii::$app->request->post('MemberSchedule')[$trainingKey][$scheduleKey];
                        $currentModel = $schedulesArray[$trainingKey][$scheduleKey];
                        $currentModel->is_subscribed = (int) $postData['is_subscribed'];
                        $currentModel->update();
                    }
                }
                Yii::$app->session->setFlash('training-saved-success', Yii::t('app', 'Enregistrement effectué'));
                return $this->refresh();
            }
        }

        return $this->render('training-weekly', [
            'trainings' => $trainings,
            'specialTrainings' => $specialTrainings,
            'schedulesArray' => $schedulesArray,
            'specialSchedulesArray' => $specialSchedulesArray,
            'memberPricing' => $memberPricing,
            'memberSpecialPricing' => $memberSpecialPricing,
            'member' => $this->member,
        ]);
    }

    /**
     * Save member subscription
     * @param  [type] $action [description]
     * @return [type]         [description]
     */
    public function actionSubscription()
    {
        $pricing = Pricing::find()
            ->where(['id' => $this->member->member_type])->one();

        $memberPricingModel = $this->getMemberPricingModel(false);
        $memberSpecialPricingModel = $this->getMemberPricingModel(true);

        if (!isset($memberPricingModel)) {
            $memberPricingModel = new MemberPricingRate();
        }

        if (!isset($memberSpecialPricingModel)) {
            $memberSpecialPricingModel = new MemberPricingRate();
        }

        if ($postData = Yii::$app->request->post('MemberPricingRate')) {

            if ((bool) Yii::$app->request->get('is_special')) {

                foreach ($postData as $data) {

                    if (!empty($data['jdls_pricing_rate_id'])) {
                        $pricingRate = PricingRate::findOne((int) $data['jdls_pricing_rate_id']);
                        $memberSpecialPricingModel->jdls_pricing_rate_id = $pricingRate->id;
                        $memberSpecialPricingModel->amount = $pricingRate->amount;
                    }
                }

                if ($memberSpecialPricingModel->isNewRecord) {
                    $memberSpecialPricingModel->jdls_member_detail_id = $this->member->id;
                    $memberSpecialPricingModel->is_special = 1;
                    $memberSpecialPricingModel->save();
                } else {
                    $memberSpecialPricingModel->update();
                }

            } else {

                foreach ($postData as $index => $data) {

                    if (!empty($data['jdls_pricing_rate_id'])) {
                        $pricingRate = PricingRate::findOne((int) $data['jdls_pricing_rate_id']);
                        $memberPricingModel->jdls_pricing_rate_id = $pricingRate->id;
                        $memberPricingModel->amount = $pricingRate->amount;
                    }

                }

                if ($memberPricingModel->isNewRecord) {
                    $memberPricingModel->jdls_member_detail_id = $this->member->id;
                    $memberPricingModel->is_special = 0;
                    $memberPricingModel->save();
                } else {
                    $memberPricingModel->update();
                }
            }

            Yii::$app->session->setFlash('subscription-saved-success', Yii::t('app', 'Enregistrement effectué'));
            return $this->redirect(['subscription']);
        }

        return $this->render('subscription', [
            'pricing' => $pricing,
            'member' => $this->member,
            'memberPricingModel' => $memberPricingModel,
            'memberSpecialPricingModel' => $memberSpecialPricingModel,
        ]);

    }

    /**
     * Validation member subscriptions
     * @return [type] [description]
     */
    public function actionValidate()
    {
        $subscriptionID = Yii::$app->request->get('id');
        $isSpecial = (bool) Yii::$app->request->get('is_special');
        $link = Url::toRoute(['payment', 'id' => $subscriptionID], true);

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['subscription']);
        } else {
            $pricingModel = MemberPricingRate::find()
                ->andWhere(['jdls_pricing_rate_id' => $subscriptionID])
                ->andWhere(['jdls_member_detail_id' => $this->member->id])->one();

            $pricingModel->is_tos_accepted = true;
            $pricingModel->save();
        }

        if ($isSpecial) {
            $tos = $this->settings->tos_special;
        } else {
            $tos = $this->settings->tos;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['link' => $link, 'settings' => $tos];
    }

    /**
     * Subscription payment
     * @param  [type] $q [description]
     * @return [type]    [description]
     */
    public function actionPayment()
    {

        $Id = Yii::$app->request->get('id');
        $invoiceId = Yii::$app->request->get('invoice_id');

        $hasSubscriptionFee = false;
        $amount = 0;

        if (!(!isset($Id) || !isset($invoiceId))) {
            return $this->redirect(['subscription']);
        }

        if (isset($invoiceId)) {
            $invoice = Invoice::findOne((int) $invoiceId);
            $amount = $invoice->amount;
            $pricingModel = MemberPricingRate::find()
                ->andWhere(['jdls_pricing_rate_id' => $invoice->rate_id])
                ->andWhere(['jdls_member_detail_id' => $this->member->id])->one();
        } else {
            $hasSubscriptionFee = true;
            $invoice = null;

            $pricingModel = MemberPricingRate::find()
                ->andWhere(['jdls_pricing_rate_id' => (int) $Id])
                ->andWhere(['jdls_member_detail_id' => $this->member->id])->one();

            $amount = $pricingModel->rate->amount + $this->settings->subscription_cost;
        }

        if (Yii::$app->request->post('stripeToken')) {

            \Stripe\Stripe::setApiKey('sk_test_1d5KRJHOfubjxlBIxHhEU1dq');

            $stripeToken = Yii::$app->request->post('stripeToken');

            $charge = \Stripe\Charge::create(array(
                'amount' => ($amount * 100),
                'currency' => 'CHF',
                'description' => 'Abonnement ' . $pricingModel->rate->period->pricing->category . ' - ' . $this->member->first_name . ' ' . $this->member->last_name,
                'source' => $stripeToken,
            ));

            if ($charge->paid) {
                if (isset($Id)) {
                    $pricingModel->status = 1;
                    $pricingModel->detail = Json::encode($charge);
                    $pricingModel->update();
                    $this->actionGenerateInvoice((int) $Id);
                } elseif (isset($invoiceId)) {
                    $invoice->status = 1;
                    $invoice->save();
                }
                return $this->redirect(['invoice']);
            };

        }

        return $this->render('payment', [
            'pricingModel' => $pricingModel,
            'member' => $this->member,
            'settings' => $this->settings,
            'hasSubscriptionFee' => $hasSubscriptionFee,
            'amount' => $amount,
            'invoice' => $invoice,
        ]);
    }

    /**
     * [actionGenerateInvoice description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function actionGenerateInvoice($id)
    {

        $pricing = MemberPricingRate::find()
            ->andWhere(['jdls_pricing_rate_id' => $id])
            ->andWhere(['jdls_member_detail_id' => $this->member->id])
            ->one();

        $start_date = Carbon::now('Europe/Zurich');

        $rateFrequency = $pricing->rate->frequency;
        $periodFrequency = $pricing->rate->period->frequency;
        $monthDelay = ($periodFrequency / $rateFrequency);

        $startDate = $start_date;
        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addMonth($monthDelay);

        for ($invoiceCount = 1; $invoiceCount <= $pricing->rate->frequency; $invoiceCount++) {

            $invoice = new Invoice();

            $invoice->jdls_member_id = $this->member->id;
            $invoice->rate_id = $pricing->rate->id;
            $invoice->reference = 'FD_' . Carbon::now()->timestamp . '_' . $invoiceCount . $this->member->id;

            if ($pricing->is_special) {
                $invoice->is_special = 1;
            } else {
                $invoice->is_special = 0;
            }

            if ($invoiceCount == 1) {
                $invoice->has_subscription_fee = 1;
                $invoice->amount = $pricing->rate->amount + $this->settings->subscription_cost;

                $invoice->start_date = $startDate;
                $invoice->end_date = $endDate;

                $startDate = $invoice->start_date;
                $endDate = $invoice->end_date;

                $invoice->status = 1;

            } else {
                $invoice->has_subscription_fee = 0;
                $invoice->amount = $pricing->rate->amount;

                $invoice->start_date = Carbon::createFromFormat('Y-m-d H:i:s', $endDate)->addDay(1);
                $invoice->end_date = Carbon::createFromFormat('Y-m-d H:i:s', $invoice->start_date)->addMonth($monthDelay);

                $startDate = $invoice->start_date;
                $endDate = $invoice->end_date;
                $invoice->status = 0;
            }

            $invoice->start_date = $invoice->start_date->timestamp;
            $invoice->end_date = $invoice->end_date->timestamp;

            $invoice->save();
        }

    }

    /**
     * View invoice as PDF
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function actionViewInvoice($id)
    {

        Yii::$app->response->format = 'pdf';
        $invoice = Invoice::findOne((int) $id);

        $pricing = MemberPricingRate::find()
            ->andWhere(['jdls_pricing_rate_id' => $invoice->rate_id])
            ->andWhere(['jdls_member_detail_id' => $this->member->id])
            ->one();

        $this->layout = '//print';

        return $this->render('invoice_pdf', [
            'settings' => $this->settings,
            'member' => $this->member,
            'invoice' => $invoice,
            'pricing' => $pricing,
        ]);

    }

    /**
     * Show all invoices and view as PDF or proceed to payment
     * @return [type] [description]
     */
    public function actionInvoice()
    {
        $invoices = $this->getInvoices(false);
        $invoicesSpecial = $this->getInvoices(true);

        return $this->render('invoice', ['invoices' => $invoices, 'invoicesSpecial' => $invoicesSpecial]);
    }

    /**
     * User settings update
     * @return [type] [description]
     */
    public function actionSettings()
    {
        $form = $this->make(SettingsForm::class);
        $event = $this->make(UserEvent::class, [$form->getUser()]);

        $this->make(AjaxRequestModelValidator::class, [$form])->validate();

        if ($form->load(Yii::$app->request->post())) {
            $this->trigger(UserEvent::EVENT_BEFORE_ACCOUNT_UPDATE, $event);

            if ($form->save()) {
                Yii::$app->getSession()->setFlash('saved-success', Yii::t('app', 'Les informations de votre compte ont été mis à jour'));
                $this->trigger(UserEvent::EVENT_AFTER_ACCOUNT_UPDATE, $event);
                return $this->refresh();
            }
        }
        return $this->render('settings', ['model' => $form]);
    }

    public function actionSendNotification()
    {
        // Yii::$app->mailer->htmlLayout = '@app/views/user/mail/layouts/html';
        //
        // $this->layout = '@app/views/user/mail/layouts/html';

       // return $this->render('@app/views/user/mail/schedule', ['member' => $this->member]);
        /*return Yii::$app->mailer
            ->compose(['html' => '@app/views/user/mail/schedule'], ['member' => $this->member])
            ->setFrom(['hello@jdl-stack.it' => 'Fight-District'])
            ->setTo('j.david.legrand@gmail.com')
            ->setSubject('Inscription Ouverte')
            ->send();*/

        if (Yii::$app->request->isAjax) {

            $data = Yii::$app->request->post('data');

            \Yii::$app->mailer->htmlLayout = '@app/views/user/mail/layouts/html';

            Yii::$app->mailer
                ->compose(['html' => '@app/views/user/mail/schedule'], ['member' => $this->member])
                ->setTo('j.david.legrand@gmail.com')
                ->setSubject('Inscription Ouverte')
                ->send();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $data;
        }
    }

}
