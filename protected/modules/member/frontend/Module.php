<?php

namespace app\modules\member\frontend;

/**
 * Member Admin Module.
 *
 * File has been created with `module/create` command on LUYA version 1.0.0-RC4.
 */
class Module extends \luya\base\Module
{
	public $layout = '@app/modules/member/frontend/views/layouts/main';
}
