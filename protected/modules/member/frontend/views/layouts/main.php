<?php
use yii\helpers\Html;
use app\assets\MemberResourcesAsset;

MemberResourcesAsset::register($this);

/* @var $this luya\web\View */
/* @var $content string */

?>

<?= $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->composition->language; ?>">
    <head>
        <meta charset="UTF-8" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?= Html::csrfMetaTags() ?>
        <title><?= $this->title; ?></title>
        <?php $this->head(); ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>

        <div id="fd-admin" class="wrapper">
            
            <div class="sidebar" data-color="red">
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="<?= Yii::$app->homeUrl; ?>" class="simple-text">FIGHT-DISTRICT</a>
                    </div>
                    <ul class="nav">
                        <li <?= (Yii::$app->controller->action->id == 'index') ? 'class="active"' : '' ?>>
                            <?= Html::a('<i class="pe-7s-id" aria-hidden="true"></i><p>' . Yii::t('app', 'Mon profil') . '</p>', ['/member/index']) ?>
                        </li>
                        <li <?= (Yii::$app->controller->action->id == 'subscription' || Yii::$app->controller->action->id == 'payment') ? 'class="active"' : '' ?>>
                            <?= Html::a('<i class="pe-7s-note2" aria-hidden="true"></i><p>' . Yii::t('app', 'Mes abonnements') . '</p>', ['/member/subscription']) ?>
                        </li>
                        <li <?= (Yii::$app->controller->action->id == 'training') ? 'class="active"' : '' ?>>
                            <?= Html::a('<i class="pe-7s-gym" aria-hidden="true"></i><p>' . Yii::t('app', 'Mes cours') . '</p>', ['/member/training']) ?>
                        </li>
                        <li <?= (Yii::$app->controller->action->id == 'invoice') ? 'class="active"' : '' ?>>
                            <?= Html::a('<i class="pe-7s-credit" aria-hidden="true"></i><p>' . Yii::t('app', 'Mes factures') . '</p>', ['/member/invoice']) ?>
                        </li>
                        <li <?= (Yii::$app->controller->action->id == 'settings') ? 'class="active"' : '' ?>>
                            <?= Html::a('<i class="pe-7s-config" aria-hidden="true"></i><p>' . Yii::t('app', 'Mes paramètres') . '</p>', ['/member/settings']) ?>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <?= Html::a('<p>Déconnexion</p>', ['/user/security/logout'], ['data-method' => 'post']); ?>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="content">
                    <div class="container-fluid">
                        <?= $content; ?>
                    </div>
                </div>

            </div>
        </div>

    <?php $this->endBody(); ?>
    <script type="text/javascript">
        window._urq = window._urq || [];
        _urq.push(['initSite', '1b1fb1c1-33f4-42dc-a8e6-f1c908fdea58']);
        (function() {
        var ur = document.createElement('script'); ur.type = 'text/javascript'; ur.async = true;
        ur.src = ('https:' == document.location.protocol ? 'https://cdn.userreport.com/userreport.js' : 'http://cdn.userreport.com/userreport.js');
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ur, s);
        })();
    </script>
    </body>
</html>
<?php $this->endPage(); ?>
