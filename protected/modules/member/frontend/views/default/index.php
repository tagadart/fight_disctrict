<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use dosamigos\datepicker\DatePicker;

use app\modules\pricing\models\Pricing;

$this->title = Yii::t('app', 'Fight-District | Mon profil');

?>

<div class="row">

    <div class="col-md-10">
        
        <?php if ($flash = Yii::$app->session->getFlash('validation-success')): ?>
            <div class="alert-tmp alert alert-success">
                <p><?= $flash ?></p>
            </div>
        <?php endif; ?>

        <?php if ($flash = Yii::$app->session->getFlash('saved-success')): ?>
            <div class="alert-tmp alert alert-success">
                <p><?= $flash ?></p>
            </div>
        <?php endif; ?>

        <!-- Profile section -->
        <div class="card">
            <div class="header">
                <h4 class="title">Données Personnelles</h4>
            </div>
            <div class="content profile-wrapper">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <?= $form->field($member, 'gender', ['options' => [ 'class' => 'col-md-12']])
                        ->radioList([
                            1 => Yii::t('app', 'Mme.'),
                            2 => Yii::t('app', 'M.')
                        ]);
                    ?>
                </div>

                <div class="row">
                    <?= $form->field($member, 'member_type', ['options' => [ 'class' => 'col-md-12']])
                        ->dropDownList(ArrayHelper::map(Pricing::find()->asArray()->all(), 'id', 'category'), ['disabled' => true])
                    ?>
                </div>

                <div class="row">
                    <?= $form->field($member, 'first_name', ['options' => [ 'class' => 'col-md-6']])->textInput() ?>
                    <?= $form->field($member, 'last_name', ['options' => [ 'class' => 'col-md-6']])->textInput() ?>
                </div>

                <div class="row">
                    <?= $form->field($member, 'date_of_birth', ['options' => [ 'class' => 'col-md-12']])
                        ->widget(\yii\widgets\MaskedInput::className(), ['name' => 'date_of_birth',  'clientOptions' => ['alias' =>  'mm.dd.yyyy']]) ?>
                </div>

                <div class="row">
                    <?= $form->field($member, 'phone_fix', ['options' => [ 'class' => 'col-md-6']])->textInput() ?>
                    <?= $form->field($member, 'phone_mobile', ['options' => [ 'class' => 'col-md-6']])->textInput() ?>
                </div>

                <?= Html::a('Enregistrer mes données', ['/member'], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>

                <div class="clearfix"></div>

                <?php ActiveForm::end(); ?>


            </div>
        </div>
        <!-- /Profile section -->

        <!-- Address Section -->
        <div class="card">
            <div class="header">
                <h4 class="title">Adresse</h4>
            </div>
            <div class="content address-wrapper">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($address, 'jdls_member_id')->hiddenInput(['value'=> $member->id])->label(false); ?>

                <div class="row">
                    <?= $form->field($address, 'street', ['options' => [ 'class' => 'col-md-12']])->textInput() ?>
                </div>

                <div class="row">
                    <?= $form->field($address, 'supplement', ['options' => [ 'class' => 'col-md-12']])->textInput() ?>
                </div>

                <div class="row">
                    <?= $form->field($address, 'npa', ['options' => [ 'class' => 'col-md-6']])->textInput() ?>
                    <?= $form->field($address, 'locality', ['options' => [ 'class' => 'col-md-6']])->textInput() ?>
                </div>

                <div class="row">
                    <?= $form->field($address, 'canton', ['options' => [ 'class' => 'col-md-12']])->textInput() ?>
                </div>

                <?= Html::a('Enregistrer mon adresse', ['/member'], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>

                <div class="clearfix"></div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <!-- /Address Section -->

    </div>

    <!-- Right Section
    <div class="col-md-4">
        <div class="card card-user">
            <div class="image">
                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
            </div>
            <div class="content">
                <div class="author">
                    <a href="#">
                    <img class="avatar border-gray" src="http://fight-district.ch/wp-content/uploads/2015/10/cabal-fight-district-438x512.jpg?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>

                    <h4 class="title"><?= $member->first_name . ' ' . $member->last_name ?><br />
                        <small><?= Yii::$app->user->identity->username ?></small>
                    </h4>
                    </a>
                </div>
            </div>
        </div>
    </div>-->
    <!-- /Right Section -->

</div>