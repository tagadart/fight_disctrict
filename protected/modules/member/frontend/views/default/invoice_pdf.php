<?php

use Carbon\Carbon;

$date = Carbon::now()->setTimezone('Europe/Zurich');

$this->title = Yii::t('app', 'Fight-District | PDF');

?>

<div class="container">

	<div id="pdf-view">

		<div class="company-detail">
			<h1 class="title"><?= $settings->title ?></h1>
			<div><?= $settings->street ?></div>
			<div>CH-<?= $settings->npa ?> <?= $settings->locality ?></div>
		</div>

		<br>
		<br>

		<div class="recipient-detail">
			<h1><?= $member->first_name ?> <?= $member->last_name ?></h1>
			<div><?= $member->address->street ?></div>
			<div>CH-<?= $member->address->npa ?> <?= $member->address->locality ?></div>
		</div>

		<br>
		<br>

		<div class="date">
			<div class="pull-right"><?= $settings->locality ?>, le <?= Yii::$app->formatter->asDate($date) ?></div>
		</div>

		<br>
		<br>
		<br>
		
		<div class="row">
			<h4 class="bold">Facture <?= $invoice->reference ?> </h4>
		</div>

		<br>
		<br>
		
		<?php if ($invoice->has_subscription_fee): ?>
		<div class="row">
			<div class="bold" style="float: left;">Frais d'inscription</div>
			<div class="right" style="float: right;">CHF <?= number_format($settings->subscription_cost, 2, '.', '') ?>.-</div>
		</div>
		<?php endif ?>
			
		<br>
		<div class="row">
			<div class="bold"><?= $pricing->rate->period->description ?></div> (<?= Yii::$app->formatter->asDate($invoice->start_date) ?> - <?= Yii::$app->formatter->asDate($invoice->end_date) ?>)
			<div class="right">CHF <?= number_format($pricing->rate->amount, 2, '.', '') ?>.-</div>
		</div>

		<hr>
		
		<div class="row">
			<span class="bold">Montant Payé</span>
			<span class="bold">CHF <?= number_format($invoice->amount, 2, '.', '') ?>.-</span>
		</div>

	</div>
</div>
