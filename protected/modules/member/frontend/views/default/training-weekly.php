<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\modules\member\models\MemberSchedule;

$count = MemberSchedule::find()
    ->andWhere(['jdls_member_detail_id' => $member->id])
    ->andWhere(['is_subscribed' => true])
    ->andWhere(['is_special_training' => false])
    ->count();

$specialCount = MemberSchedule::find()
    ->andWhere(['jdls_member_detail_id' => $member->id])
    ->andWhere(['is_subscribed' => true])
    ->andWhere(['is_special_training' => true])
    ->count();

$formatter = \Yii::$app->formatter;

$this->title = Yii::t('app', 'Fight-District | Mes Cours');

$currentDay = $this->params['currentDay'];

?>

<div class="row">

    <div class="col-md-12">
        
        <?php if ($flash = Yii::$app->session->getFlash('training-saved-success')): ?>
            <div class="alert-tmp alert alert-success">
                <p><?= $flash ?></p>
            </div>
        <?php endif; ?>

        <nav class="week-days">
            <ul>
                <?php foreach($this->params['days'] as $key => $day): ?>
                <li <?= ($currentDay == $key) ? 'class="active"' : '' ?>>
                    <?= Html::a($day, ['/member/default/training-weekly', 'day' => $key], ['class' => 'btn']); ?>        
                </li>
                <?php endforeach; ?>
            </ul>
        </nav>

        <div class="card">
            <div class="header">
                <h4 class="title">Horaires des Cours | <span>Inscris à <?= $count ?> cours</span></h4>
            </div>
            <div class="content training-wrapper">

                <?php if (!(bool)$memberPricing->status): ?>
                <div class="alert alert-danger">
                    <p>Vous n'avez aucun abonnement en cours, veuillez vous souscrire à un abonnement sous <b>Mes abonnements</b></p>
                </div>
                <?php endif; ?>

                <?php $form = ActiveForm::begin(); ?>
				<?php foreach($trainings as $tIndex => $training): ?>

                <?php if (sizeof($training->getSchedulesByDay($currentDay)) > 0): ?>
				<div class="row">
					<div class="col-md-3 left-column">
						<div class="training-tile">
							<span class="<?= $training->icon; ?>"></span>
							<div><?= $training->title ?></div>
						</div>
					</div>
					<div class="col-md-9 right-column">
						<table class="table">                             
                            <tbody>
                                <?php if (!empty($training->getSchedulesByDay($currentDay))): ?>

                                    <?php foreach($training->getSchedulesByDay($currentDay) as $sIndex => $schedule): ?>

                                        <tr class="<?= $schedule->count > 0 ? 'unblocked-row': 'blocked-row' ?>">

                                            <td class="ckb-container">

                                                <?php if (($schedule->count == 0 && (bool)$schedulesArray[$tIndex][$sIndex]['is_subscribed']) || $schedule->count > 0): ?>

                                                   <?= $form->field($schedulesArray[$tIndex][$sIndex], "[$tIndex][$sIndex]is_subscribed", [
                                                            'template' => "<div class=\"checkbox\">{input} \n <label for=\"memberschedule-{$tIndex}-{$sIndex}-is_subscribed\"></label> </div>"
                                                        ])->checkbox(['label' => null, 'disabled' => !(bool)$memberPricing->status])
                                                    ?>

                                                <?php else: ?>
                                                    <span class="pe-7s-lock"></span>
                                                <?php endif; ?>
                                            </td>

                                            <td><?= $formatter->asDate($schedule->start_time, 'HH:mm') . ' - ' . $formatter->asDate($schedule->end_time, 'HH:mm') ?></td>
                                            
                                            <?php if ($schedule->count > 0): ?>
                                                <td><?= $schedule->count ?> places</td>
                                            <?php else: ?>
                                                <td>Complet</td>
                                            <?php endif ?>
                                        </tr>

                                    <?php endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td><span class="alert alert-info">L'horaire du cour n'est pas encore défini</span></td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
					</div>
				</div>
                <?php endif; ?>
				
                <?php endforeach; ?>

                <?php if ((bool)$memberPricing->status): ?>
                <?= Html::a('Enregistrer', ['/member/default/training', 'q' => false], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>
                <div class="clearfix"></div>
                <?php endif; ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        
        <?php if($member->member_type != 1): ?>
        <!-- Special training -->
        <div class="card">
             <div class="header">
                <h4 class="title">Horaires de Cross-Training | <span>Inscris à <?= $specialCount ?> cours</span></h4>
            </div>
            <div class="content training-wrapper special">
                
                <?php if (!(bool)$memberSpecialPricing->status): ?>
                <div class="alert alert-danger">
                    <p>Vous n'avez aucun abonnement en cours, veuillez vous souscrire à l'abonnement Cross-Training sous <b>Mes abonnements</b></p>
                </div>
                <?php endif; ?>

                <?php $form = ActiveForm::begin(
                    [   
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true
                    ]
                ); ?>
                <?php foreach($specialTrainings as $tIndex => $training): ?>

                <?php if (sizeof($training->getSchedulesByDay(1)) > 0): ?>
                <div class="row">
                    <div class="col-md-3 left-column">
                        <div class="training-tile">
                            <span class="<?= $training->icon; ?>"></span>
                            <div><?= $training->title ?></div>
                        </div>
                    </div>
                    <div class="col-md-9 right-column">
                        <table class="table">                             
                            <tbody>
                                <?php if (!empty($training->getSchedulesByDay(1))): ?>
                                    <?php foreach($training->getSchedulesByDay(1) as $sIndex => $schedule): ?>
                                        <tr class="<?= $schedule->count > 0 ? 'unblocked-row': 'blocked-row' ?>">
                                            <td>
                                                <?= $form->field($specialSchedulesArray[$tIndex][$sIndex], "[$tIndex][$sIndex]is_subscribed")
                                                    ->checkbox(['label' => null, 'disabled' => !(bool)$memberSpecialPricing->status])
                                                ?>
                                            </td>
                                            <td><?= $schedule->translatedDay ?></td>
                                            <td><?= $formatter->asDate($schedule->start_time, 'HH:mm') . ' - ' . $formatter->asDate($schedule->end_time, 'HH:mm') ?></td>
                                            <?php if ($schedule->count > 0): ?>
                                            <td><?= $schedule->count ?> places disponibles</td>
                                            <?php else: ?>
                                            <td>Complet</td>
                                            <?php endif ?>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td>
                                            <span class="alert alert-info">L'horaire du cour n'est pas encore défini</span>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php endif ?>
                <?php endforeach; ?>

                <?php if ((bool)$memberSpecialPricing->status): ?>
                <?= Html::a('Enregistrer', ['/member/default/training', 'q' => true], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>
                <div class="clearfix"></div>
                <?php endif; ?>

                <?php ActiveForm::end(); ?>
            </div>
            <!-- /Special training -->
        </div>
        <?php endif ?>
    </div>
</div>