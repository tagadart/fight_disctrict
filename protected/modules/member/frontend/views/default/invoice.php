<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$formatter = \Yii::$app->formatter;

$this->title = Yii::t('app', 'Fight-District | Mes Factures');

?>

<div class="row">

    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Mes Factures | <span>Cours Généraux</span></h4>
            </div>
            <div class="content table-responsive table-full-width invoice-wrapper">
                <table class="table table-hover table-striped">
                    <thead>
                    	<th>Référence</th>
                    	<th>Début</th>
                    	<th>Fin</th>
                    	<th>Montant</th>
                    	<th>Statut</th>
                        <th>Actions</th>
                        <th>PDF</th>
                    </thead>
                    <tbody>
                        <?php foreach($invoices as $invoice): ?>
                        <tr>
                        	<td>
                                <?= $invoice->reference ?>
                                <?= $invoice->is_special ? ' | Cross-training' : '| Autres cours' ?>
                            </td>
                        	<td><?= $invoice->start_date == null ? '-' : Yii::$app->formatter->asDate($invoice->start_date)  ?></td>
                        	<td><?= $invoice->end_date == null ? '-' : Yii::$app->formatter->asDate($invoice->end_date)  ?></td>
                        	<td><?= $invoice->amount ?>.-</td>
                            <td><?= $invoice->statusText ?></td>
                        	<td>
                                <?php if (!$invoice->status): ?>
                                <?= Html::a('<span class="pe-7s-cash"></span>', ['payment', 'invoice_id' => $invoice->id], ['class' => 'btn btn-success btn-fill', 'data-method' => 'post']); ?>
                                <?php else: ?>
                                <span></span>
                                <?php endif ?>
                            </td>
                        	<td>
                                <?php if ($invoice->status): ?>
                                <?= Html::a('<span class="pe-7s-file"></span>', ['view-invoice', 'id' => $invoice->id], ['target' => '_blank']); ?>
                                <?php else: ?>
                                <span>-</span>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <?php if($member->member_type != 1): ?>
        <?php if(!empty($invoicesSpecial)): ?>
        <div class="card">
            <div class="header">
                <h4 class="title">Mes Factures | <span>Cross-Training</span></h4>
            </div>
            <div class="content table-responsive table-full-width invoice-wrapper">
                <table class="table table-hover table-striped">
                    <thead>
                        <th>Référence</th>
                        <th>Début</th>
                        <th>Fin</th>
                        <th>Montant</th>
                        <th>Statut</th>
                        <th>Actions</th>
                        <th>PDF</th>
                    </thead>
                    <tbody>
                        <?php foreach($invoicesSpecial as $invoice): ?>
                        <tr>
                            <td>
                                <?= $invoice->reference ?>
                                <?= $invoice->is_special ? ' | Cross-training' : '| Autres cours' ?>
                            </td>
                            <td><?= $invoice->start_date == null ? '-' : Yii::$app->formatter->asDate($invoice->start_date)  ?></td>
                            <td><?= $invoice->end_date == null ? '-' : Yii::$app->formatter->asDate($invoice->end_date)  ?></td>
                            <td><?= $invoice->amount ?>.-</td>
                            <td><?= $invoice->statusText ?></td>
                            <td>
                                <?php if (!$invoice->status): ?>
                                <?= Html::a('<span class="pe-7s-cash"></span>', ['payment', 'invoice_id' => $invoice->id], ['class' => 'btn btn-success btn-fill', 'data-method' => 'post']); ?>
                                <?php else: ?>
                                <span></span>
                                <?php endif ?>
                            </td>
                            <td>
                                <?php if ($invoice->status): ?>
                                <?= Html::a('<span class="pe-7s-file"></span>', ['view-invoice', 'id' => $invoice->id], ['target' => '_blank']); ?>
                                <?php else: ?>
                                <span>-</span>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php endif ?>
        <?php endif ?>

    </div>
</div>