<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$formatter = \Yii::$app->formatter;

$this->title = Yii::t('app', 'Fight-District | Mes Paramètres');

?>

<div class="row">

    <div class="col-md-10">

        <?php if ($flash = Yii::$app->session->getFlash('saved-success')): ?>
            <div class="alert-tmp alert alert-success">
                <p><?=$flash?></p>
            </div>
        <?php endif;?>

        <div class="card">
            <div class="header">
                <h4 class="title">Mes Paramètres</h4>
            </div>
            <div class="content settings-wrapper">

                <?php $form = ActiveForm::begin(
                    [
                        'id' => $model->formName(),
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                    ]);
                ?>

                <div class="row">
                    <?=$form->field($model, 'email', ['options' => ['class' => 'col-md-12']])->textInput(['disabled' => 'true'])?>
                </div>

                <div class="row">
                    <?=$form->field($model, 'current_password', ['options' => ['class' => 'col-md-6']])->passwordInput()?>
                    <?=$form->field($model, 'new_password', ['options' => ['class' => 'col-md-6']])->passwordInput()?>
                </div>

                <?=Html::a('Enregistrer', ['/member/settings'], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']);?>

                <div class="clearfix"></div>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>