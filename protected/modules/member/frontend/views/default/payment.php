<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use ruskid\stripe\StripeCheckout;

$formatter = \Yii::$app->formatter;

$this->title = Yii::t('app', 'Fight-District | Paiement');

if (!isset($invoice)) {
	$url = ['payment', 'id' => $pricingModel->jdls_pricing_rate_id];
} else {
	$url = ['payment', 'invoice_id' => $invoice->id];
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content payment-wrapper">
            	<h3>Récapitulatifs des coûts</h3>
            	<h4>Type de membre: <?= $pricingModel->rate->period->pricing->category ?></h4>
				<?php if ($pricingModel->rate->is_special): ?>	
            	<h5 class="text-center">Abonnement Cross-Training</h5>
            	<?php else: ?>
				<h5 class="text-center">Abonnement général sans Cross-Training</h5>
				<?php endif; ?>

				<div class="detail">

					<?php if ($hasSubscriptionFee): ?>
	            	<div class="row">
	            		<span>Frais d'inscription</span>
	            		<span class="pull-right"><?= number_format($settings->subscription_cost, 2, '.', '') ?></span>
	            	</div>
	            	<?php endif ?>

	            	<div class="row">
	            		<span>
	            			<?php if (!isset($invoice)): ?>
	            				<span><?= $pricingModel->rate->period->description ?></span>
	            				<span class="alert alert-warning">Paiement en <?= $pricingModel->rate->frequency ?> acompte(s) de CHF <?= $pricingModel->amount ?></span>
	            			<?php else: ?>
	            				<span><?= $pricingModel->rate->period->description ?></span>
	            				<span class="alert alert-warning">Paiement pour la période du <?= $formatter->asDate($invoice->start_date) ?> au <?= $formatter->asDate($invoice->end_date) ?></span>
	            			<?php endif ?>
	            		</span>
	            		<span class="pull-right">CHF <?= number_format($pricingModel->amount, 2, '.', '') ?></span>
	            	</div>

	            	<div class="row total">
	            		<span>Montant à payer</span>
	            		<span class="pull-right">CHF <?= number_format($amount, 2, '.', '') ?></span>
	            	</div>
            	</div>
            	<div class="clearfix"></div>
            	<div class="row stripe-btn">
            		<div class="col-md-12">
	            		<?=
	            			StripeCheckout::widget([
							    'action' => Url::to($url),
							    'name' => 'Abonnement',
							    'description' => '',
							    'label' => 'Payer mon abonnement',
							    'userEmail' => Yii::$app->user->identity->email,
							    'allowRemember' => false,
							    'currency' => 'CHF',
							    'panelLabel' => 'Payer',
							    'amount' => ($amount * 100),
							    'image' => 'https://stripe.com/img/documentation/checkout/marketplace.png',
							]);
	            		?>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>