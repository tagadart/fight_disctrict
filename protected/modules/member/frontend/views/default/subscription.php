<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use ruskid\stripe\StripeCheckout;

$formatter = \Yii::$app->formatter;

$this->title = Yii::t('app', 'Fight-District | Mes Abonnements');

?>

<div class="row">

    <div class="col-md-12">
    	<?php if ($flash = Yii::$app->session->getFlash('subscription-saved-success')): ?>
            <div class="alert-tmp alert alert-success">
                <p><?= $flash ?></p>
            </div>
        <?php endif; ?>
        <div class="card">
            <div class="header">
                <h4 class="title">Mon Abonnement | <span>Fight</span></h4>
            </div>
            <div class="content subscription-wrapper">

            	<?php if ((bool)$memberPricingModel->status): ?>
            	<div class="alert alert-info">
                	<p>Vous n'êtes plus en mesure de modifier cet abonnement ! Veuillez prendre contact avec notre service clientèle</p>
            	</div>
            	<?php endif; ?>

            	<?php $form = ActiveForm::begin(); ?>
	    		<div class="row">
	        		<?php foreach($pricing->periods as $index => $period): ?>
						<div class="col-md-4">
							<h3><?= $period->description ?></h3>
							<ul>
							<?php foreach($period->rates as $rate): ?>
								<li>
									<?= $form->field($memberPricingModel, '[]jdls_pricing_rate_id', ['errorOptions' => ['tag' => null]])
	                                    ->radioList([$rate->id => ''], ['itemOptions' => ['disabled' => (bool)$memberPricingModel->status]])
	                                    ->label(false);
	                                ?>
	                                <?= $rate->frequency ?> x CHF <?= $rate->amount ?>
                                </li>
							<?php endforeach; ?>
							</ul>
						</div>
	            	<?php endforeach; ?>
	        	</div>

	        	<?php if (!(bool)$memberPricingModel->status): ?>
		        	<?= Html::a('Valider', ['/member/subscription', 'is_special' => false], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>
		        	<?= Html::a('Procéder au paiement', ['/member/validate', 'id' => $memberPricingModel->jdls_pricing_rate_id, 'is_special' => false], ['class' => 'training-btn btn btn-warning btn-fill pull-right']); ?>
		            <div class="clearfix"></div>
	        	<?php endif; ?>

	            <?php ActiveForm::end(); ?>
            </div>
        </div>
		
		<?php if($member->member_type != 1): ?>
		<!-- Special pricing -->
		<div class="card">
			<div class="header">
                <h4 class="title">Mon Abonnement | <span>Cross-Training</span></h4>
            </div>
            <div class="content subscription-wrapper special">
                <?php if ((bool)$memberSpecialPricingModel->status): ?>
            	<div class="alert alert-info">
                	<p>Vous n'êtes plus en mesure de modifier cet abonnement ! Veuillez prendre contact avec notre service à la clientèle</p>
            	</div>
            	<?php endif; ?>
            	<?php $form = ActiveForm::begin(); ?>
	    		<div class="row">
	        		<?php foreach($pricing->periods as $index => $period): ?>
						<div class="col-md-4">
							<h3><?= $period->description ?></h3>
							<ul>
							<?php foreach($period->specialRates as $rate): ?>
								<li>
									<?= $form->field($memberSpecialPricingModel, '[]jdls_pricing_rate_id', ['errorOptions' => ['tag' => null]])
		                                    ->radioList([$rate->id => ''], ['itemOptions' => ['disabled' => (bool)$memberSpecialPricingModel->status]])
		                                    ->label(false);
	                                ?>
	                                <?= $rate->frequency ?> x CHF <?= $rate->amount ?>
                                </li>
							<?php endforeach; ?>
							</ul>
						</div>
	            	<?php endforeach; ?>
	        	</div>

	        	<?php if (!(bool)$memberSpecialPricingModel->status): ?>
		        	<?= Html::a('Valider', ['/member/subscription', 'is_special' => true], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>
		        	<?= Html::a('Procéder au paiement', ['/member/validate', 'id' => $memberSpecialPricingModel->jdls_pricing_rate_id, 'is_special' => true], ['class' => 'training-btn btn btn-warning btn-fill pull-right']); ?>
		            <div class="clearfix"></div>
	            <?php endif; ?>
	            <?php ActiveForm::end(); ?>
            </div>
        </div>
        <!-- /Special pricing -->
        <?php endif; ?>
    </div>
</div>