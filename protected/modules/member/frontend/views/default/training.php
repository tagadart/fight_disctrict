<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use Carbon\Carbon;

use russ666\widgets\Countdown;

use app\modules\member\models\MemberSchedule;

function calculateDelay($day, $startTime) {

    $daysOfWeek = [1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday', 7 => 'sunday'];
    $now = Carbon::now('Europe/Zurich');

    $time = Carbon::parse(\Yii::$app->formatter->asDate($startTime, 'HH:mm'));

    $openingCountDown = Carbon::parse($daysOfWeek[$day]);
    $closingCountDown = Carbon::parse($daysOfWeek[$day]);

    $openingCountDown->addHours($time->hour);
    $openingCountDown->addMinutes($time->minute);

    $openingCountDown->hour -= 63;
    $openingCountDown->minute -= 59;

    if ($openingCountDown > $now) {
        // return $closingCountDown;
    } else {
        // return $openingCountDown;
    }

    return $openingCountDown;

}

$count = MemberSchedule::find()
    ->andWhere(['jdls_member_detail_id' => $member->id])
    ->andWhere(['is_subscribed' => true])
    ->andWhere(['is_special_training' => false])
    ->count();

$specialCount = MemberSchedule::find()
    ->andWhere(['jdls_member_detail_id' => $member->id])
    ->andWhere(['is_subscribed' => true])
    ->andWhere(['is_special_training' => true])
    ->count();

$formatter = \Yii::$app->formatter;

$this->title = Yii::t('app', 'Fight-District | Mes Cours');

?>

<script type="text/javascript">
    function sendRequest(data) {
        $.ajax({
            url: '<?= \Yii::$app->getUrlManager()->createUrl('member/send-notification') ?>',
            type: 'POST',
            data: { 'data': data },
            success: function(res) {
                alert(res);
            }
        });
    }
</script>

<div class="row">

    <div class="col-md-12">
        
        <?php if ($flash = Yii::$app->session->getFlash('training-saved-success')): ?>
            <div class="alert-tmp alert alert-success">
                <p><?= $flash ?></p>
            </div>
        <?php endif; ?>

        <div class="card">
            <div class="header">
                <h4 class="title">Horaires des Cours | <span>Inscris à <?= $count ?> cours</span></h4>
            </div>
            <div class="content training-wrapper">

                <?php if (!(bool)$memberPricing->status): ?>
                <div class="custom-alert alert alert-with-icon">
                    <span data-notify="icon" class="pe-7s-lock"></span>
                    <span>Vous n'avez aucun abonnement en cours, veuillez vous souscrire à un abonnement sous <b>Mes abonnements</b></span>
                </div>
                <?php endif; ?>

                <?php $form = ActiveForm::begin(); ?>
				<?php foreach($trainings as $tIndex => $training): ?>
				<div class="row">
					<div class="col-md-3 left-column">
						<div class="training-tile">
							<span class="<?= $training->icon; ?>"></span>
							<div><?= $training->title ?></div>
						</div>
					</div>
					<div class="col-md-9 right-column">

						<?php foreach($this->params['days'] as $key => $day): ?>
                            
                            <?php if (in_array($key, ArrayHelper::getColumn($training->schedules, 'training_day'))): ?>

                            <h4><?= $day ?></h4>

                            <div class="schedules-wrapper">
                                <?php foreach($training->schedules as $sIndex => $schedule): ?>
                                    <?php if ($schedule->training_day == $key): ?>
                                        <div class="schedule-row <?= $schedule->count > 0 ? 'unblocked-row': 'blocked-row' ?>">

                                            <?php if (($schedule->count == 0 && (bool)$schedulesArray[$tIndex][$sIndex]['is_subscribed']) || $schedule->count > 0): ?>
                                            <?= $form->field($schedulesArray[$tIndex][$sIndex], "[$tIndex][$sIndex]is_subscribed", [
                                                    'template' => "<div class=\"checkbox\">{input} \n <label for=\"memberschedule-{$tIndex}-{$sIndex}-is_subscribed\"></label> </div>"
                                                ])->checkbox(['label' => null, 'disabled' => !(bool)$memberPricing->status])
                                            ?>
                                            <?php else: ?>
                                                <span class="form-group pe-7s-lock"></span>
                                            <?php endif; ?>
                                            
                                            <div><?= $formatter->asDate($schedule->start_time, 'HH:mm') . ' - ' . $formatter->asDate($schedule->end_time, 'HH:mm') ?></div>

                                            <?php if ($schedule->count > 0): ?>
                                                <div style="width: 10rem;"><?= $schedule->count ?> places</div>
                                            <?php else: ?>
                                                <div style="width: 10rem;">Complet</div>
                                            <?php endif ?>
                                            
                                            <?php if ($schedule->count > 0): ?>
                                            <div style="width: 30rem;">
                                                <span class="badge">Inscription ouverte dans
                                                    <?php echo Countdown::widget([
                                                        'datetime' => calculateDelay($schedule->training_day, $schedule->start_time),
                                                        'format' => '%-d j %H h %M m %S',
                                                        'events' => [
                                                            'finish' => 'function(){ sendRequest(' . json_encode([
                                                                'is_subscribed' => $schedulesArray[$tIndex][$sIndex]['is_subscribed'],
                                                                'schedule_id' => $schedule->id
                                                            ]) . ') }',
                                                        ],
                                                    ]) ?>
                                                </span>
                                            </div>
                                            <?php endif ?>

                                        </div>
                                    <?php endif ?>
                                <?php endforeach; ?>
                            </div>
                            <?php endif; ?>

                        <?php endforeach; ?>
					</div>
				</div>
				<?php endforeach; ?>

                <?php if ((bool)$memberPricing->status): ?>
                <?= Html::a('Enregistrer', ['/member/default/training', 'q' => false], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>
                <div class="clearfix"></div>
                <?php endif; ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        
        <?php if($member->member_type != 1): ?>
        <!-- Special training -->
        <div class="card">
             <div class="header">
                <h4 class="title">Horaires de Cross-Training | <span>Inscris à <?= $specialCount ?> cours</span></h4>
            </div>
            <div class="content training-wrapper special">
                
                <?php if (!(bool)$memberSpecialPricing->status): ?>
                <div class="custom-alert alert alert-with-icon">
                    <span data-notify="icon" class="pe-7s-lock"></span>
                    <span>Vous n'avez aucun abonnement en cours, veuillez vous souscrire à l'abonnement Cross-Training sous <b>Mes abonnements</b></span>
                </div>
                <?php endif; ?>

                <?php $form = ActiveForm::begin(
                    [   
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => true
                    ]
                ); ?>
                <?php foreach($specialTrainings as $tIndex => $training): ?>
                <div class="row">
                    <div class="col-md-3 left-column">
                        <div class="training-tile">
                            <span class="<?= $training->icon; ?>"></span>
                            <div><?= $training->title ?></div>
                        </div>
                    </div>
                    <div class="col-md-9 right-column">

                        <?php foreach($this->params['days'] as $key => $day): ?>
                            
                            <?php if (in_array($key, ArrayHelper::getColumn($training->schedules, 'training_day'))): ?>

                            <h4><?= $day ?></h4>

                            <div class="schedules-wrapper">
                                <?php foreach($training->schedules as $sIndex => $schedule): ?>
                                    <?php if ($schedule->training_day == $key): ?>
                                        <div class="schedule-row <?= $schedule->count > 0 ? 'unblocked-row': 'blocked-row' ?>">

                                            <?php if (($schedule->count == 0 && (bool)$specialSchedulesArray[$tIndex][$sIndex]['is_subscribed']) || $schedule->count > 0): ?>
                                            <?= $form->field($specialSchedulesArray[$tIndex][$sIndex], "[$tIndex][$sIndex]is_subscribed", [
                                                    'template' => "<div class=\"checkbox\">{input} \n <label for=\"memberschedule-{$tIndex}-{$sIndex}-is_subscribed\"></label> </div>"
                                                ])->checkbox(['label' => null, 'disabled' => !(bool)$memberSpecialPricing->status])
                                            ?>
                                            <?php else: ?>
                                                <span class="form-group pe-7s-lock"></span>
                                            <?php endif; ?>
                                            
                                            <div><?= $formatter->asDate($schedule->start_time, 'HH:mm') . ' - ' . $formatter->asDate($schedule->end_time, 'HH:mm') ?></div>

                                            <?php if ($schedule->count > 0): ?>
                                                <div style="width: 10rem;"><?= $schedule->count ?> places</div>
                                            <?php else: ?>
                                                <div style="width: 10rem;">Complet</div>
                                            <?php endif ?>
                                            
                                            <?php if ($schedule->count > 0): ?>
                                            <div style="width: 30rem;">
                                                <span>Inscription ouverte dans </span>
                                                <?php echo Countdown::widget([
                                                    'datetime' => calculateDelay($schedule->training_day, $schedule->start_time),
                                                    'format' => '%-d jours %M:%S',
                                                    'events' => [
                                                        'finish' => 'function(){location.reload()}',
                                                    ],
                                                ]) ?>
                                            </div>
                                            <?php endif ?>

                                        </div>
                                    <?php endif ?>
                                <?php endforeach; ?>
                            </div>
                            <?php endif; ?>

                        <?php endforeach; ?>

                    </div>
                </div>
                <?php endforeach; ?>

                <?php if ((bool)$memberSpecialPricing->status): ?>
                <?= Html::a('Enregistrer', ['/member/default/training', 'q' => true], ['class' => 'btn btn-success btn-fill pull-right', 'data-method' => 'post']); ?>
                <div class="clearfix"></div>
                <?php endif; ?>

                <?php ActiveForm::end(); ?>
            </div>
            <!-- /Special training -->
        </div>
        <?php endif ?>
    </div>
</div>