<?php

namespace app\modules\member\admin\controllers;

/**
 * Member Pricing Rate Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class MemberPricingRateController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\member\models\MemberPricingRate';
}