<?php

namespace app\modules\member\admin;

/**
 * Member Admin Module.
 *
 * File has been created with `module/create` command on LUYA version 1.0.0-RC4. 
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
	    'api-member-detail' => 'app\modules\member\admin\apis\MemberController',
	    'api-pricing-invoice' => 'app\modules\pricing\admin\apis\InvoiceController',
	    'api-member-memberschedule' => 'app\modules\member\admin\apis\MemberScheduleController',
	    'api-member-memberpricingrate' => 'app\modules\member\admin\apis\MemberPricingRateController',
	    'api-member-user' => 'app\modules\member\admin\apis\UserController',
	    'api-member-csvimport' => 'app\modules\member\admin\apis\CsvImportController',
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Membres', 'people')
	        	->group('Group')
	        		->itemApi('Liste des membres', 'memberadmin/member/index', 'label', 'api-member-detail')
	        		->itemApi('Liste des factures', 'pricingadmin/invoice/index', 'label', 'api-pricing-invoice')
	        		->itemApi('Import CSV', 'memberadmin/csv-import/index', 'label', 'api-member-csvimport')
	        		->itemApi('User', 'memberadmin/user/index', 'label', 'api-member-user', ['hiddenInMenu' => true])
	        		->itemApi('Cours', 'memberadmin/member-schedule/index', 'label', 'api-member-memberschedule', ['hiddenInMenu' => true])
	        		->itemApi('Abonnements', 'memberadmin/member-pricing-rate/index', 'label', 'api-member-memberpricingrate', ['hiddenInMenu' => true])
	        		->itemApi('Factures', 'pricingadmin/invoice/index', 'label', 'api-pricing-invoice', ['hiddenInMenu' => true]);
	}

}
