<?php

namespace app\modules\member\admin\apis;

/**
 * Member Schedule Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class MemberScheduleController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\member\models\MemberSchedule';
}