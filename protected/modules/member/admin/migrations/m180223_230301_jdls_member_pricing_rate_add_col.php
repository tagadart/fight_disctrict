<?php

use yii\db\Migration;

/**
 * Class m180223_230301_jdls_member_pricing_rate_add_col
 */
class m180223_230301_jdls_member_pricing_rate_add_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jdls_member_princing_rate', 'has_subscription_fee', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_member_princing_rate', 'has_subscription_fee');
    }
    
}
