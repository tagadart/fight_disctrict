<?php

use yii\db\Migration;

/**
 * Class m180220_074801_jdls_member_pricing_add_status_col
 */
class m180220_074801_jdls_member_pricing_add_status_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_member_princing_rate', 'status', $this->boolean()->defaultValue(false));
        $this->addColumn('jdls_member_princing_rate', 'is_tos_accepted', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->dropColumn('jdls_member_princing_rate', 'status');
       $this->dropColumn('jdls_member_princing_rate', 'is_tos_accepted');
    }

}
