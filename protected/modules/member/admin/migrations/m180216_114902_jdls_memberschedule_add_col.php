<?php

use yii\db\Migration;

/**
 * Class m180216_114902_jdls_memberschedule_add_col
 */
class m180216_114902_jdls_memberschedule_add_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_member_schedule', 'is_subscribed', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_member_schedule', 'is_subscribed');
    }

}
