<?php

use yii\db\Migration;

/**
 * Class m180214_020035_jdls_member_remove_col
 */
class m180214_020035_jdls_member_remove_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('jdls_member_detail', 'salutation');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }

}
