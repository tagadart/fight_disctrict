<?php

use yii\db\Migration;

/**
 * Class m180214_123621_jdls_member_schedule_tbl
 */
class m180214_123621_jdls_member_schedule_tbl extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_member_schedule', [
            'jdls_member_detail_id' => $this->integer()->notNull(),
            'jdls_schedule_id' => $this->integer()->notNull(),
            'detail' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-jdls_member_schedule-jdls_member_detail_id',
            'jdls_member_schedule',
            'jdls_member_detail_id',
            'jdls_member_detail',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-jdls_member_schedule-jdls_schedule_id',
            'jdls_member_schedule',
            'jdls_schedule_id',
            'jdls_schedule',
            'id',
            'CASCADE'
        );

        $this->addPrimaryKey('pk-jdls_member_detail_jdls_schedule', 'jdls_member_schedule', ['jdls_member_detail_id', 'jdls_schedule_id']);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('jdls_member_schedule');

        $this->dropForeignKey(
            'fk-jdls_member_schedule-jdls_member_detail_id',
            'jdls_member_schedule'
        );

        $this->dropForeignKey(
            'fk-jdls_member_schedule-jdls_schedule_id',
            'jdls_member_schedule'
        );

        $this->dropPrimaryKey(
            'pk-jdls_member_detail_jdls_schedule', 
            'jdls_member_schedule'
        );
    }

}
