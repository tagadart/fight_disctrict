<?php

use yii\db\Migration;

/**
 * Class m180212_234720_jdls_member_add_col
 */
class m180212_234720_jdls_member_add_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('jdls_member_detail', 'firs_tname', 'first_name');

        $this->addColumn('jdls_member_detail', 'member_type', $this->integer());
        $this->addColumn('jdls_member_detail', 'date_of_birth', $this->date());
        $this->addColumn('jdls_member_detail', 'gender', $this->integer());
        $this->addColumn('jdls_member_detail', 'phone_fix', $this->string());
        $this->addColumn('jdls_member_detail', 'phone_mobile', $this->string());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_member_detail', 'firs_tname');

        $this->dropColumn('jdls_member_detail', 'member_type');
        $this->dropColumn('jdls_member_detail', 'date_of_birth');
        $this->dropColumn('jdls_member_detail', 'gender');
        $this->dropColumn('jdls_member_detail', 'phone_fix');
        $this->dropColumn('jdls_member_detail', 'phone_mobile');
    }
}
