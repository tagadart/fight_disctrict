<?php

use yii\db\Migration;

/**
 * Class m180226_172556_jdls_member_add_fk
 */
class m180226_172556_jdls_member_add_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-jdls_member_detail-user_id',
            'jdls_member_detail',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-jdls_member_detail-user_id',
            'jdls_member_detail'
        );
    }

}
