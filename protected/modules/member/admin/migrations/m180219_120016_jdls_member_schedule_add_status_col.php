<?php

use yii\db\Migration;

/**
 * Class m180219_120016_jdls_member_schedule_add_status_col
 */
class m180219_120016_jdls_member_schedule_add_status_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_member_schedule', 'is_special_training', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_member_schedule', 'is_special_training');
    }
    
}
