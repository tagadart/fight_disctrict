<?php

use yii\db\Migration;

/**
 * Class m180225_144054_add_csv_import_tbl
 */
class m180225_144054_add_csv_import_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('jdls_csv_import', [
            'id' => $this->primaryKey(),
            'csv_file' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('jdls_csv_import');
    }

}
