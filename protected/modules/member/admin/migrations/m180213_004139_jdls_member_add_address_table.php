<?php

use yii\db\Migration;

/**
 * Class m180213_004139_jdls_member_add_address_table
 */
class m180213_004139_jdls_member_add_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_address', [
            'id' => $this->primaryKey(),
            'jdls_member_id' => $this->integer()->notNull(),
            'street' => $this->string()->notNull(),
            'supplement' => $this->string()->notNull(),
            'npa' => $this->string()->notNull(),
            'locality' => $this->string()->notNull(),
            'canton' => $this->string()->notNull()
        ]);

        $this->addForeignKey(
            'fk-jdls_address-jdls_member_id',
            'jdls_address',
            'jdls_member_id',
            'jdls_member_detail',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('jdls_address');

        $this->dropForeignKey(
            'fk-jdls_address-jdls_member_id',
            'jdls_address'
        );
    }

}
