<?php

use yii\db\Migration;

/**
 * Class m180226_174531_jdls_member_add_col
 */
class m180226_174531_jdls_member_add_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jdls_member_detail', 'is_deleted', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_member_detail', 'is_deleted');
    }

}
