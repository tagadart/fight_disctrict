<?php

use yii\db\Migration;

/**
 * Class m180219_140420_jdls_member_pricing_rate_add_status_col
 */
class m180219_140420_jdls_member_pricing_rate_add_status_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_member_princing_rate', 'is_special', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_member_princing_rate', 'is_special');
    }

}
