<?php

use yii\db\Migration;

/**
 * Class m180224_123130_jdls_member_add_ts_behavior
 */
class m180224_123130_jdls_member_add_ts_behavior extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jdls_address', 'created_at', $this->integer());
        $this->addColumn('jdls_address', 'updated_at', $this->integer());

        $this->addColumn('jdls_member_detail', 'created_at', $this->integer());
        $this->addColumn('jdls_member_detail', 'updated_at', $this->integer());

        $this->addColumn('jdls_member_princing_rate', 'created_at', $this->integer());
        $this->addColumn('jdls_member_princing_rate', 'updated_at', $this->integer());

        $this->addColumn('jdls_member_schedule', 'created_at', $this->integer());
        $this->addColumn('jdls_member_schedule', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_address', 'created_at');
        $this->dropColumn('jdls_address', 'updated_at');

        $this->dropColumn('jdls_member_detail', 'created_at');
        $this->dropColumn('jdls_member_detail', 'updated_at');

        $this->dropColumn('jdls_member_princing_rate', 'created_at');
        $this->dropColumn('jdls_member_princing_rate', 'updated_at');

        $this->dropColumn('jdls_member_schedule', 'created_at');
        $this->dropColumn('jdls_member_schedule', 'updated_at');
    }

}
