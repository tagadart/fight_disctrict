<?php

use yii\db\Migration;

class m171111_131849_jdls_member_detail extends Migration
{
    public function safeUp()
    {
        $this->createTable('jdls_member_detail', [
            'id' => $this->primaryKey(),
            'salutation' => $this->string(),
            'firs_tname' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('jdls_member_detail');
    }
}
