<?php

use yii\db\Migration;

/**
 * Class m180216_191018_jdls_member_princing_rate_add_tbl
 */
class m180216_191018_jdls_member_princing_rate_add_tbl extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_member_princing_rate', [
            'id' => $this->primaryKey(),
            'jdls_member_detail_id' => $this->integer()->notNull(),
            'jdls_pricing_rate_id' => $this->integer()->notNull(),
            'description' => $this->string(),
            'amount' => $this->double(),
            'detail' => $this->string(),
            'is_special' => $this->boolean()->defaultValue(false),
            'is_tos_accepted' => $this->boolean()->defaultValue(false),
            'has_subscription_fee' => $this->boolean()->defaultValue(false),
            'status' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-jdls_member_princing_rate-jdls_member_detail_id',
            'jdls_member_princing_rate',
            'jdls_member_detail_id',
            'jdls_member_detail',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-jdls_member_princing_rate-jdls_pricing_rate_id',
            'jdls_member_princing_rate',
            'jdls_pricing_rate_id',
            'jdls_pricing_rate',
            'id',
            'CASCADE'
        );

        // $this->addPrimaryKey('pk-jdls_member_detail_jdls_princing_rate', 'jdls_member_princing_rate', ['jdls_member_detail_id', 'jdls_pricing_rate_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('jdls_member_princing_rate');

        $this->dropForeignKey(
            'fk-jdls_member_princing_rate-jdls_member_detail_id',
            'jdls_member_princing_rate'
        );

        $this->dropForeignKey(
            'fk-jdls_member_princing_rate-jdls_pricing_rate_id',
            'jdls_member_princing_rate'
        );

        /*$this->dropPrimaryKey(
            'pk-jdls_member_detail_jdls_princing_rate', 
            'jdls_member_princing_rate'
        );*/
    }

}
