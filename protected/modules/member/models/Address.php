<?php

namespace app\modules\member\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\member\models\Member;

use yii\behaviors\TimestampBehavior;

/**
 * Address.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $jdls_member_id
 * @property string $street
 * @property string $supplement
 * @property string $npa
 * @property string $locality
 * @property string $canton
 */
class Address extends NgRestModel
{

    use SoftDeleteTrait;

     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_address';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-member-address';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jdls_member_id' => Yii::t('app', 'Jdls Member ID'),
            'street' => Yii::t('app', 'Rue et numéro'),
            'supplement' => Yii::t('app', 'Complément d\'adresse'),
            'npa' => Yii::t('app', 'NPA'),
            'locality' => Yii::t('app', 'Localité'),
            'canton' => Yii::t('app', 'Canton'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jdls_member_id', 'street', 'npa', 'locality', 'canton'], 'required'],
            [['jdls_member_id'], 'integer'],
            [['street', 'supplement', 'npa', 'locality', 'canton'], 'string', 'max' => 255],
            [['jdls_member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['jdls_member_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['street', 'supplement', 'npa', 'locality', 'canton'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'jdls_member_id' => 'number',
            'street' => 'text',
            'supplement' => 'text',
            'npa' => 'text',
            'locality' => 'text',
            'canton' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['jdls_member_id', 'street', 'supplement', 'npa', 'locality', 'canton']],
            [['create', 'update'], ['jdls_member_id', 'street', 'supplement', 'npa', 'locality', 'canton']],
            ['delete', false],
        ];
    }
}