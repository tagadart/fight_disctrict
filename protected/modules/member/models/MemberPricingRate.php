<?php

namespace app\modules\member\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\member\models\Member;
use app\modules\pricing\models\PricingRate;

use yii\behaviors\TimestampBehavior;

/**
 * Member Pricing Rate.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $jdls_member_detail_id
 * @property integer $jdls_pricing_rate_id
 * @property double $amount
 * @property string $detail
 */
class MemberPricingRate extends NgRestModel
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_member_princing_rate';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-member-memberpricingrate';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jdls_member_detail_id' => Yii::t('app', 'Membre'),
            'jdls_pricing_rate_id' => Yii::t('app', 'Type'),
            'amount' => Yii::t('app', 'Montant/Acompte'),
            'detail' => Yii::t('app', 'Detail'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Statut'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jdls_member_detail_id', 'jdls_pricing_rate_id', 'is_special', 'status', 'is_tos_accepted', 'has_subscription_fee'], 'integer'],
            [['amount'], 'number'],
            [['detail', 'description'], 'string'],
            [['jdls_member_detail_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['jdls_member_detail_id' => 'id']],
            [['jdls_pricing_rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => PricingRate::className(), 'targetAttribute' => ['jdls_pricing_rate_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['detail'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'jdls_member_detail_id' => 'number',
            'jdls_pricing_rate_id' => [
                'selectModel',
                'modelClass' => PricingRate::className(),
                'valueField' => 'id',
                'labelField' => function($model) {
                    $detail = $model->is_special ? 'CT ' : 'Fight ';
                    return  $model->period->pricing->category . ': ' . $detail . ' | ' . $model->period->description . ' | ' . $model->frequency . ' acompte(s)';
                }
            ],
            'amount' => 'decimal',
            'detail' => 'text',
            'description' => 'text',
            'status' => ['toggleStatus', 'initValue' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['jdls_pricing_rate_id', 'amount', 'status']],
            [['create', 'update'], ['jdls_member_detail_id', 'jdls_pricing_rate_id', 'amount', 'status']],
            ['delete', true],
        ];
    }

    public function getRate()
    {
        return $this->hasOne(PricingRate::class, ['id' => 'jdls_pricing_rate_id']);
    }

}