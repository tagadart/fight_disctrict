<?php

namespace app\modules\member\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;
use luya\admin\ngrest\plugins\SelectRelationActiveQuery;

use app\modules\member\models\User;
use app\modules\member\models\Address;
use app\modules\member\models\MemberPricingRate;
use app\modules\member\models\MemberSchedule;
use app\modules\pricing\models\Pricing;
use app\modules\pricing\models\PricingRate;
use app\modules\pricing\models\Invoice;

use app\modules\schedule\models\Schedule;

use yii\behaviors\TimestampBehavior;

/**
 * Member.
 * 
 * File has been created with `crud/create` command on LUYA version 1.0.0-RC4. 
 *
 * @property integer $id
 * @property string $firs_tname
 * @property string $last_name
 */
class Member extends NgRestModel
{

    use SoftDeleteTrait;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_member_detail';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-member-detail';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_type' => Yii::t('app', 'Catégorie'),
            'first_name' => Yii::t('app', 'Prénom'),
            'last_name' => Yii::t('app', 'Nom'),
            'gender' => Yii::t('app', 'Genre'),
            'date_of_birth' => Yii::t('app', 'Date de naissance'),
            'phone_fix' => Yii::t('app', 'Téléphone Fixe'),
            'phone_mobile' => Yii::t('app', 'Téléphone Mobile'),
            'user_id' => Yii::t('app', 'Email'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'date_of_birth', 'gender', 'member_type'], 'required'],
            [['user_id'], 'integer'],
            [['first_name', 'last_name', 'phone_fix', 'phone_mobile'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['first_name', 'last_name'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'gender' => ['selectArray', 'data' => [1 => 'Madame', 2 => 'Monsieur']],
            'member_type' => [
                'selectModel',
                'modelClass' => Pricing::className(),
                'valueField' => 'id',
                'labelField' => 'category'
            ],
            'user_id' => [
                'class' => SelectRelationActiveQuery::class, 
                'query' => $this->getUser(), 
                'labelField' => ['email']
            ],
            'first_name' => 'text',
            'last_name' => 'text',
            'date_of_birth' => 'date',
            'phone_fix' => 'text',
            'phone_mobile' => 'text'
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['first_name', 'last_name', 'member_type', 'date_of_birth']],
            // [['create'], ['gender', 'member_type', 'first_name', 'last_name', 'date_of_birth', 'phone_fix', 'phone_mobile']],
            ['create', []],
            ['update', ['gender', 'member_type', 'user_id', 'first_name', 'last_name', 'date_of_birth', 'phone_fix', 'phone_mobile']],
            ['delete', true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestRelations()
    {
        return [
            ['label' => 'Cours', 'apiEndpoint' => Schedule::ngRestApiEndpoint(), 'dataProvider' => $this->getSchedules()],
            ['label' => 'Abonnements', 'apiEndpoint' => MemberPricingRate::ngRestApiEndpoint(), 'dataProvider' => $this->getMemberships()],
            ['label' => 'Factures', 'apiEndpoint' => Invoice::ngRestApiEndpoint(), 'dataProvider' => $this->getInvoices()]
        ];
    }

    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['jdls_member_id' => 'id']);
    }

    public function getSchedules() {
        return $this->hasMany(Schedule::className(), ['id' => 'jdls_schedule_id'])
            ->viaTable('jdls_member_schedule', ['jdls_member_detail_id' => 'id'], function ($query) {
                $query->andWhere(['is_subscribed' => true]);
            });
    }

    public function getMemberships() {
        return $this->hasMany(MemberPricingRate::className(), ['jdls_member_detail_id' => 'id']);
    }

    public function getAddress() {
        return $this->hasOne(Address::className(), ['jdls_member_id' => 'id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
