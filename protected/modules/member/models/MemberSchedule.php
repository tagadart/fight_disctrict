<?php

namespace app\modules\member\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\member\models\Member;
use app\modules\schedule\models\Schedule;

use yii\behaviors\TimestampBehavior;

/**
 * Member Schedule.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $jdls_member_detail_id
 * @property integer $jdls_schedule_id
 * @property string $detail
 */
class MemberSchedule extends NgRestModel
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_member_schedule';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-member-memberschedule';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jdls_member_detail_id' => Yii::t('app', 'Membre'),
            'jdls_schedule_id' => Yii::t('app', 'Cours'),
            'detail' => Yii::t('app', 'Détail'),
            'is_subscribed' => Yii::t('app', 'Statut'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jdls_member_detail_id', 'jdls_schedule_id', 'is_subscribed', 'is_special_training'], 'integer'],
            [['detail'], 'string', 'max' => 255],
            [['jdls_member_detail_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['jdls_member_detail_id' => 'id']],
            [['jdls_schedule_id'], 'exist', 'skipOnError' => true, 'targetClass' => Schedule::className(), 'targetAttribute' => ['jdls_schedule_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['detail'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'jdls_schedule_id' => 'number',
            'detail' => 'text',
            'is_subscribed' => ['toggleStatus', 'initValue' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['detail', 'is_subscribed']],
            [['create', 'update'], ['jdls_schedule_id', 'detail', 'is_subscribed']],
            ['delete', false],
        ];
    }
}