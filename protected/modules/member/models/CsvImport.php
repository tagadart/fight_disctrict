<?php

namespace app\modules\member\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\base\Exception;

use luya\admin\ngrest\base\NgRestModel;
use luya\cms\helpers\BlockHelper;
use luya\admin\traits\SoftDeleteTrait;

use Da\User\Form\RegistrationForm;
use Da\User\Model\User;
use Da\User\Model\Profile;
use Da\User\Traits\ContainerAwareTrait;
use Da\User\Service\UserRegisterService;

use app\modules\member\models\Member;

use Carbon\Carbon;

/**
 * Csvimport.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $csv_file
 * @property integer $created_at
 * @property integer $updated_at
 */
class CsvImport extends NgRestModel
{

    use ContainerAwareTrait, SoftDeleteTrait;

     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_csv_import';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-member-csvimport';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'csv_file' => Yii::t('app', 'Fichier CSV'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['csv_file'], 'required'],
            [['csv_file'], 'safe'],
            [['csv_file'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return [''];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'csv_file' => ['file', 'fileItem' => true]
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['csv_file']],
            [['update'], ['csv_file']],
            ['delete', false],
        ];
    }

    private function registerMembers($members)
    {
        
        foreach ($members as $member) {

            $date = Carbon::now();
            $random = Carbon::createFromFormat('Y-m-d H:i:s', $date)->timestamp;

            $registrationForm = new RegistrationForm();
            $newMember = new Member();

            $username = strtolower($member->first_name) . '.' . strtolower($member->last_name) . '.' . $random;

            $registrationForm->email = $member->email;
            $registrationForm->username = $username;
            $registrationForm->password = "*FightDistrict_{$random}#";

            $newMember->first_name = $member->first_name;
            $newMember->last_name = $member->last_name;
            $newMember->date_of_birth = (int) Carbon::createFromFormat('d.m.Y', $member->date_of_birth)->timestamp;
            $newMember->member_type = (int) $member->member_type;
            $newMember->gender = (int) $member->gender;
            $newMember->phone_fix = $member->phone_fix;
            $newMember->phone_mobile = $member->phone_mobile;

            $user = $this->make(User::class, [], $registrationForm->attributes);
            $user->confirmed_at = $random;

            try {
                if ($user->validate()) {
                    $user->save();
                    $newMember->user_id = $user->id;
                    if ($newMember->validate()) {
                        $newMember->save();
                    }
                }
            } catch (Exception $e) {
                $this->addError($attributs, 'Veillez vérifier les données du CSV');
                throw $e;
            }
        }
    }

    public function beforeSave($insert)
    {
        if (!$insert) {
            try {
                $uploadFile = BlockHelper::fileUpload($this->csv_file, true);
                $csvData = file($uploadFile->getSourceAbsolute());
                $csvData = array_slice($csvData, 1);
                $members = [];
                $gender = ['Femme' => 1, 'Homme' => 2];
                $type = ['Adulte' => 3, 'Etudiant' => 2, 'Enfant' => 1];
                $fieldsMapping = [0 => 'gender', 1 => 'member_type', 2 => 'first_name', 3 => 'last_name', 4 => 'date_of_birth', 5 => 'email', 6 => 'phone_mobile', 7 => 'phone_fix'];

                foreach ($csvData as $key => $data) {
                    $member = [];
                    $explodeDatas = explode(',', $data);
                    foreach ($fieldsMapping as $fieldKey => $field) {
                       if ($field == 'gender') {
                            $member[$field] = $gender[trim($explodeDatas[$fieldKey])];
                       } else if ($field == 'member_type') {
                            $member[$field] = $type[trim($explodeDatas[$fieldKey])];
                       } else {
                            $member[$field] = trim($explodeDatas[$fieldKey]) == '' ? null : trim($explodeDatas[$fieldKey]);
                       }                    
                    }
                    $members[] = (object)$member;
                }
            } catch (Exception $e) {
                $this->addError($attributs, 'Veillez vérifier les données du CSV');
            }
        }
        $this->registerMembers($members);
        return parent::beforeSave($insert);
    }
}
