<?php

namespace app\modules\settings\admin;

/**
 * Settings Admin Module.
 *
 * File has been created with `module/create` command. 
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
    	'api-settings-settings' => 'app\modules\settings\admin\apis\SettingsController'
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Configuration', 'build')
	        ->group('Group')
	        ->itemApi('Paramètres Généraux', 'settingsadmin/settings/index', 'label', 'api-settings-settings');
	}
}