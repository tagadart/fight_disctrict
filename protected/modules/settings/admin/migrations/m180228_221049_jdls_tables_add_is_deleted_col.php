<?php

use yii\db\Migration;

/**
 * Class m180228_221049_jdls_tables_add_is_deleted_col
 */
class m180228_221049_jdls_tables_add_is_deleted_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /* Module Member */
        $this->addColumn('jdls_address', 'is_deleted', $this->boolean()->defaultValue(false));
        $this->addColumn('jdls_csv_import', 'is_deleted', $this->boolean()->defaultValue(false));
        $this->addColumn('jdls_member_detail', 'is_deleted', $this->boolean()->defaultValue(false));

        /* Module Pricing */
        $this->addColumn('jdls_invoice', 'is_deleted', $this->boolean()->defaultValue(false));
        $this->addColumn('jdls_pricing', 'is_deleted', $this->boolean()->defaultValue(false));
        $this->addColumn('jdls_pricing_period', 'is_deleted', $this->boolean()->defaultValue(false));
        $this->addColumn('jdls_pricing_rate', 'is_deleted', $this->boolean()->defaultValue(false));

        /* Module Schedule */
        $this->addColumn('jdls_schedule', 'is_deleted', $this->boolean()->defaultValue(false));

        /* Module Settings */
        $this->addColumn('jdls_settings', 'is_deleted', $this->boolean()->defaultValue(false));

        /* Module Team */
        $this->addColumn('jdls_team_member', 'is_deleted', $this->boolean()->defaultValue(false));

        /* Module Training */
        $this->addColumn('jdls_training', 'is_deleted', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       /* Module Member */
        $this->dropColumn('jdls_address', 'is_deleted');
        $this->dropColumn('jdls_csv_import', 'is_deleted');
        $this->dropColumn('jdls_member_detail', 'is_deleted');

        /* Module Pricing */
        $this->dropColumn('jdls_invoice', 'is_deleted');
        $this->dropColumn('jdls_pricing', 'is_deleted');
        $this->dropColumn('jdls_pricing_period', 'is_deleted');
        $this->dropColumn('jdls_pricing_rate', 'is_deleted');

        /* Module Schedule */
        $this->dropColumn('jdls_schedule', 'is_deleted');

        /* Module Settings */
        $this->dropColumn('jdls_settings', 'is_deleted');

        /* Module Team */
        $this->dropColumn('jdls_team_member', 'is_deleted');

        /* Module Training */
        $this->dropColumn('jdls_training', 'is_deleted');
    }

}
