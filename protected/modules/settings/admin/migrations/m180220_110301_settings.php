<?php

use yii\db\Migration;

/**
 * Class m180220_110301_settings
 */
class m180220_110301_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_settings', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
            'logo_url' => $this->string(),
            'image_url' => $this->string(),
            'street' => $this->string(),
            'npa' => $this->string(),
            'locality' => $this->string(),
            'canton' => $this->string(),
            'subscription_cost' => $this->double(),
            'tos' => $this->text(),
            'tos_special' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('jdls_settings');
    }

}
