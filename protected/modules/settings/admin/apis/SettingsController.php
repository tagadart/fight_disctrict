<?php

namespace app\modules\settings\admin\apis;

/**
 * Settings Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class SettingsController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\settings\models\Settings';
}