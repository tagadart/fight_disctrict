<?php

namespace app\modules\settings\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use yii\behaviors\TimestampBehavior;

/**
 * Settings.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $title
 * @property text $description
 * @property string $logo_url
 * @property string $image_url
 * @property string $street
 * @property string $npa
 * @property string $locality
 * @property string $canton
 * @property double $subscription_cost
 * @property text $tos
 * @property text $tos_special
 */
class Settings extends NgRestModel
{

    use SoftDeleteTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_settings';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-settings-settings';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Société'),
            'description' => Yii::t('app', 'Description'),
            'logo_url' => Yii::t('app', 'Logo'),
            'image_url' => Yii::t('app', 'Image'),
            'street' => Yii::t('app', 'Rue & No.'),
            'npa' => Yii::t('app', 'NPA'),
            'locality' => Yii::t('app', 'Localité'),
            'canton' => Yii::t('app', 'Canton'),
            'subscription_cost' => Yii::t('app', 'Taxe d\'inscription'),
            'tos' => Yii::t('app', 'CGV'),
            'tos_special' => Yii::t('app', 'CGV Cross-Training'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'tos', 'tos_special'], 'string'],
            [['logo_url', 'image_url'], 'integer'],
            [['subscription_cost'], 'number'],
            [['title', 'street', 'npa', 'locality', 'canton'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['title', 'description', 'logo_url', 'image_url', 'street', 'npa', 'locality', 'canton', 'tos', 'tos_special'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'title' => 'text',
            'logo_url' => ['file', 'fileItem' => true],
            'image_url' => ['file', 'fileItem' => true],
            'street' => 'text',
            'npa' => 'text',
            'locality' => 'text',
            'canton' => 'text',
            'subscription_cost' => 'decimal',
            'description' => ['textarea', 'markdown' => true],
            'tos' => ['textarea', 'markdown' => true],
            'tos_special' => ['textarea', 'markdown' => true],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['title', 'description', 'subscription_cost']],
            [['update'], ['title', 'description', 'logo_url', 'image_url', 'street', 'npa', 'locality', 'canton', 'subscription_cost', 'tos', 'tos_special']],
            ['delete', false],
        ];
    }
}