<?php

namespace app\modules\training\admin\apis;

/**
 * Training Controller.
 * 
 * File has been created with `crud/create` command on LUYA version 1.0.0-RC4. 
 */
class TrainingController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\training\models\Training';
}