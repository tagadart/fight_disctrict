<?php

use yii\db\Migration;

class m171127_062357_jdls_training extends Migration
{
    public function safeUp()
    {
        $this->createTable('jdls_training', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'subtitle' => $this->string(),
            'description' => $this->text()->notNull(),
            'icon' => $this->string()->notNull(),
            'image' => $this->string(),
            'video' => $this->string(),
            'nb_participants' => $this->integer()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('jdls_training');
    }
}
