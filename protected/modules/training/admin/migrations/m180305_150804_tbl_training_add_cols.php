<?php

use yii\db\Migration;

/**
 * Class m180305_150804_tbl_training_add_cols
 */
class m180305_150804_tbl_training_add_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jdls_training', 'video_url', $this->string()->after('video'));
        $this->addColumn('jdls_training', 'is_youtube', $this->boolean()->defaultValue(false)->after('video_url'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_training', 'video_url');
        $this->dropColumn('jdls_training', 'is_youtube');
    }

}
