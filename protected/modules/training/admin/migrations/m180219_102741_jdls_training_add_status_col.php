<?php

use yii\db\Migration;

/**
 * Class m180219_102741_jdls_training_add_status_col
 */
class m180219_102741_jdls_training_add_status_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_training', 'is_special', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_training', 'is_special');
    }

}
