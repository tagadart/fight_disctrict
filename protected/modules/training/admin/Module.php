<?php

namespace app\modules\training\admin;

/**
 * Training Admin Module.
 *
 * File has been created with `module/create` command on LUYA version 1.0.0-RC4.
 */
class Module extends \luya\admin\base\Module
{
    public $apis = [
        'api-training-training' => 'app\modules\training\admin\apis\TrainingController'
    ];

    public function getMenu()
    {
        return (new \luya\admin\components\AdminMenuBuilder($this))
            ->node('Cours', 'school')
            ->group('Group')
            ->itemApi('Liste des cours', 'trainingadmin/training/index', 'label', 'api-training-training');
    }
}
