<?php

namespace app\modules\training\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\schedule\models\Schedule;

use yii\behaviors\TimestampBehavior;

use Carbon\Carbon;

/**
 * Training.
 *
 * File has been created with `crud/create` command on LUYA version 1.0.0-RC4.
 *
 * @property int $id
 * @property string $title
 * @property string $subtitle
 * @property text $description
 * @property string $icon
 * @property string $image
 * @property string $video
 * @property int $nb_participants
 */
class Training extends NgRestModel
{

    use SoftDeleteTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jdls_training';
    }

    /**
     * {@inheritdoc}
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-training-training';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Nom du cours'),
            'item_order' => Yii::t('app', 'Position'),
            'subtitle' => Yii::t('app', 'Sous-titre'),
            'description' => Yii::t('app', 'Description'),
            'icon' => Yii::t('app', 'Icon'),
            'image' => Yii::t('app', 'Image'),
            'video' => Yii::t('app', 'Vidéo'),
            'video_url' => Yii::t('app', 'ID Youtube'),
            'is_youtube' => Yii::t('app', 'Youtube'),
            'nb_participants' => Yii::t('app', 'Nombre de participants'),
            'is_special' => Yii::t('app', 'Cours spécial'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'icon'], 'required'],
            [['description'], 'string'],
            [['nb_participants', 'image', 'video', 'item_order', 'is_special', 'is_youtube'], 'integer'],
            [['title', 'subtitle', 'icon', 'video_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function genericSearchFields()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function ngRestAttributeTypes()
    {
        return [
            'item_order' => 'number',
            'title' => 'text',
            'subtitle' => 'text',
            'description' => ['textarea', 'markdown' => true],
            'icon' => 'text',
            'image' => ['file', 'fileItem' => true],
            'video' => ['file', 'fileItem' => true],
            'video_url' => 'text',
            'nb_participants' => 'number',
            'is_special' => ['toggleStatus', 'initValue' => 0],
            'is_youtube' => ['toggleStatus', 'initValue' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['item_order', 'title']],
            [['create', 'update'], ['item_order', 'title', 'subtitle', 'description', 'is_special', 'icon', 'image', 'video', 'video_url', 'is_youtube']],
            ['delete', true],
        ];
    }

    public function getSchedulesByDay($day)
    {
        return $this->hasMany(Schedule::class, ['jdls_training_id' => 'id'])
            ->where(['training_day' => (int)$day])
            ->orderBy(['training_day' => SORT_ASC])
            ->all();
    }

    public function getSchedules()
    {
        return $this->hasMany(Schedule::class, ['jdls_training_id' => 'id'])
            ->orderBy(['training_day' => SORT_ASC])
            ->all();
    }

    public function getAssociatedArray()
    {   
        $schedules = [];
        $scheduleDays = [];
        $formatter = \Yii::$app->formatter;

        $days = [1 => 'Lundi', 2 => 'Mardi', 3 => 'Mercredi', 4 => 'Jeudi', 5 => 'Vendredi', 6 => 'Samedi', 7 => 'Dimanche'];

        $timeSchedules = $this->hasMany(Schedule::class, ['jdls_training_id' => 'id'])
            ->orderBy(['training_day' => SORT_ASC])
            ->orderBy(['start_time' => SORT_ASC])->asArray()->all();

        foreach ($timeSchedules as $scheduleKey => $timeSchedule) {
            if (!in_array($timeSchedule['training_day'], $schedules)) {
                $scheduleDays[$timeSchedule['training_day']] = $days[$timeSchedule['training_day']];
            }
        }
       
        foreach ($timeSchedules as $scheduleKey => $timeSchedule) {
            $time = $formatter->asDate($timeSchedule['start_time'], 'HH:mm') . '_' . $formatter->asDate($timeSchedule['end_time'], 'HH:mm');
            $schedules[$time][] = new Schedule($timeSchedule);
        }

        return [$schedules, $scheduleDays];

    }
    
}
