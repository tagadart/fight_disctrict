<?php

namespace app\modules\schedule\admin;

/**
 * Schedule Admin Module.
 *
 * File has been created with `module/create` command. 
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
    	'api-schedule-schedule' => 'app\modules\schedule\admin\apis\ScheduleController',
    	'api-member-detail' => 'app\modules\member\admin\apis\MemberController',
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Planification', 'today')
	        	->group('Group')
	        		->itemApi('Horaires des cours', 'scheduleadmin/schedule/index', 'label', 'api-schedule-schedule')
	        		->itemApi('Participants', 'memberadmin/member/index', 'label', 'api-member-detail', ['hiddenInMenu' => true]);
	}
}