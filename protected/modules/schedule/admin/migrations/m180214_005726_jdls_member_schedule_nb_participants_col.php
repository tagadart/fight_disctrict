<?php

use yii\db\Migration;

/**
 * Class m180214_005726_jdls_member_schedule_nb_participants_col
 */
class m180214_005726_jdls_member_schedule_nb_participants_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_schedule', 'nb_participants', $this->integer()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_schedule', 'nb_participants');
    }

}
