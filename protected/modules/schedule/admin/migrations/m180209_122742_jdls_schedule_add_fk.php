<?php

use yii\db\Migration;

/**
 * Class m180209_122742_jdls_schedule_add_fk
 */
class m180209_122742_jdls_schedule_add_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-jdls_schedule-jdls_training_id',
            'jdls_schedule',
            'jdls_training_id',
            'jdls_training',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-jdls_schedule-jdls_training_id',
            'jdls_schedule'
        );
    }

}
