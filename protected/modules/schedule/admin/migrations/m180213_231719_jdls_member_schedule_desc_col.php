<?php

use yii\db\Migration;

/**
 * Class m180213_231719_jdls_member_schedule_desc_col
 */
class m180213_231719_jdls_member_schedule_desc_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_schedule', 'description', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_schedule', 'description');
    }

}
