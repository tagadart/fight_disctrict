<?php

use yii\db\Migration;

/**
 * Class m180214_140224_jdls_schedule_add_col
 */
class m180214_140224_jdls_schedule_add_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_schedule', 'training_day', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_schedule', 'training_day');
    }

}
