<?php

use yii\db\Migration;

/**
 * Class m180207_092835_jdls_schedule
 */
class m180207_092835_jdls_schedule extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_schedule', [
            'id' => $this->primaryKey(),
            'jdls_training_id' => $this->integer()->notNull(),
            'monday' => $this->boolean(),
            'tuesday' => $this->boolean(),
            'wednesday' => $this->boolean(),
            'thursday' => $this->boolean(),
            'friday' => $this->boolean(),
            'saturday' => $this->boolean(),
            'sunday' => $this->boolean()->defaultValue(false),
            'start_time' => $this->time(),
            'end_time' => $this->time()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('jdls_schedule');
    }

}
