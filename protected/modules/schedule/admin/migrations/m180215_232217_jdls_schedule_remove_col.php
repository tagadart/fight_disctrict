<?php

use yii\db\Migration;

/**
 * Class m180215_232217_jdls_schedule_remove_col
 */
class m180215_232217_jdls_schedule_remove_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('jdls_schedule', 'monday');
        $this->dropColumn('jdls_schedule', 'tuesday');
        $this->dropColumn('jdls_schedule', 'wednesday');
        $this->dropColumn('jdls_schedule', 'thursday');
        $this->dropColumn('jdls_schedule', 'friday');
        $this->dropColumn('jdls_schedule', 'saturday');
        $this->dropColumn('jdls_schedule', 'sunday');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        
    }

}
