<?php

namespace app\modules\schedule\admin\apis;

/**
 * Schedule Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class ScheduleController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\schedule\models\Schedule';

    public $pageSize = 10;
}