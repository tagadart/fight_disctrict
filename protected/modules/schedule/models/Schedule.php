<?php

namespace app\modules\schedule\models;

use Yii;
use yii\base\Event;

use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\training\models\Training;
use app\modules\member\models\Member;
use app\modules\member\models\MemberSchedule;

use yii\behaviors\TimestampBehavior;
use Carbon\Carbon;

/**
 * Schedule.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property integer $jdls_training_id
 * @property time $start_time
 * @property time $end_time
 */
class Schedule extends NgRestModel
{
    
    use SoftDeleteTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_schedule';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-schedule-schedule';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jdls_training_id' => Yii::t('app', 'Cours'),
            'description' => Yii::t('app', 'Description'),
            'nb_participants' => Yii::t('app', 'Nb de places'),
            'subscriptionCount' => Yii::t('app', 'Nb d\'inscription'),
            'training_day' => Yii::t('app', 'Jour'),
            'start_time' => Yii::t('app', 'Heure début'),
            'end_time' => Yii::t('app', 'Heure fin'),
            'formatedStartTime' => Yii::t('app', 'Début'),
            'formatedEndTime' => Yii::t('app', 'Fin'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jdls_training_id', 'nb_participants'], 'required'],
            [['jdls_training_id', 'nb_participants', 'training_day'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['start_time', 'end_time'], 'safe'],
            [['jdls_training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['jdls_training_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['jdls_training_id', 'start_time', 'end_time'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'jdls_training_id' => [
                'selectModel',
                'modelClass' => Training::className(),
                'valueField' => 'id',
                'labelField' => 'title'
            ],
            'description' => 'text',
            'nb_participants' => 'number',
            'training_day' => [
                'selectArray',
                'data' => [
                    1 => 'Lundi',
                    2 => 'Mardi',
                    3 => 'Mercredi',
                    4 => 'Jeudi',
                    5 => 'Vendredi',
                    6 => 'Samedi',
                    7 => 'Dimanche'
                ]
            ],
            'start_time' => 'datetime',
            'end_time' => 'datetime'
        ];
    }

    public function ngrestExtraAttributeTypes()
    {
        return [
            'formatedStartTime' => 'text',
            'formatedEndTime' => 'text',
            'subscriptionCount' => 'number',
        ];
    }

    public function ngRestRelations()
    {
        return [
            ['label' => 'Participants', 'apiEndpoint' => Member::ngRestApiEndpoint(), 'dataProvider' => $this->getMembers()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['jdls_training_id', 'description', 'nb_participants', 'subscriptionCount', 'training_day', 'formatedStartTime', 'formatedEndTime']],
            [['create', 'update'], ['jdls_training_id', 'nb_participants', 'description', 'training_day', 'start_time', 'end_time']],
            ['delete', true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            foreach (Member::find()->all() as $member) {
                $memberSchedule = new MemberSchedule();
                $memberSchedule->jdls_member_detail_id = $member->id;
                $memberSchedule->jdls_schedule_id = $this->id;
                $memberSchedule->is_subscribed = 0;
                $memberSchedule->save();
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return ['formatedStartTime', 'formatedEndTime', 'subscriptionCount'];
    }

    public function getMembers() {
        return $this->hasMany(Member::className(), ['id' => 'jdls_member_detail_id'])
            ->viaTable('jdls_member_schedule', ['jdls_schedule_id' => 'id'], function ($query) {
                $query->andWhere(['is_subscribed' => true]);
            });
    }

    public function getTranslatedDay() {
        $days = [1 => 'Lundi', 2 => 'Mardi', 3 => 'Mercredi', 4 => 'Jeudi', 5 => 'Vendredi', 6 => 'Samedi', 7 => 'Dimanche'];
        return $days[$this->training_day];
    }

    public function getCount()
    {
        return $this->nb_participants - MemberSchedule::find()
            ->andWhere(['is_subscribed' => true])
            ->andWhere(['jdls_schedule_id' => $this->id])->count();
    }

    public function getSubscriptionCount()
    {
        return MemberSchedule::find()
            ->andWhere(['is_subscribed' => true])
            ->andWhere(['jdls_schedule_id' => $this->id])->count();
    }

    public function getFormatedStartTime()
    {
        return Yii::$app->formatter->asTime($this->start_time);
    }

    public function getFormatedEndTime()
    {
        return Yii::$app->formatter->asTime($this->end_time);
    }

}