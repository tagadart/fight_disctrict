<?php

namespace app\modules\pricing\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\pricing\models\PricingPeriod;

use yii\behaviors\TimestampBehavior;

/**
 * Pricing.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property string $category
 * @property text $description
 * @property double $amount
 */
class Pricing extends NgRestModel
{

    use SoftDeleteTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();       
        return $behaviors;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_pricing';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-pricing-pricing';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Catégorie'),
            'description' => Yii::t('app', 'Description'),
            'amount' => Yii::t('app', 'Montant'),
            'duration' => Yii::t('app', 'Durée'),
            'currency' => Yii::t('app', 'Devise'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'amount'], 'required'],
            [['description'], 'string'],
            [['amount'], 'number'],
            [['category', 'duration', 'currency'], 'string', 'max' => 255],
            [['duration'], 'default', 'value'=> 'Année'],
            [['currency'], 'default', 'value'=> 'CHF'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['category', 'description'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'category' => 'text',
            'duration' => 'text',
            'currency' => 'text',
            'description' => 'textarea',
            'amount' => 'decimal'
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['category', 'amount']],
            [['create', 'update'], ['category', 'description', 'amount']],
            ['delete', true],
        ];
    }

    public function getPeriods()
    {
        return $this->hasMany(PricingPeriod::class, ['jdls_pricing_id' => 'id']);
    }

}