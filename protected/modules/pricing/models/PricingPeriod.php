<?php

namespace app\modules\pricing\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\pricing\models\Pricing;
use app\modules\pricing\models\PricingRate;

use yii\behaviors\TimestampBehavior;

/**
 * Pricing Period.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $jdls_pricing_id
 * @property integer $frequency
 * @property text $description
 * @property integer $row_config
 */
class PricingPeriod extends NgRestModel
{

    use SoftDeleteTrait;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();       
        return $behaviors;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_pricing_period';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-pricing-pricingperiod';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jdls_pricing_id' => Yii::t('app', 'Catégorie'),
            'frequency' => Yii::t('app', 'Durée (en mois)'),
            'description' => Yii::t('app', 'Description'),
            'row_config' => Yii::t('app', 'Row Config'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jdls_pricing_id'], 'required'],
            [['jdls_pricing_id', 'frequency', 'row_config'], 'integer'],
            [['description'], 'string'],
            [['jdls_pricing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pricing::className(), 'targetAttribute' => ['jdls_pricing_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['description'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'jdls_pricing_id' => [
                'selectModel',
                'modelClass' => Pricing::className(),
                'valueField' => 'id',
                'labelField' => 'category'
            ],
            'frequency' => 'number',
            'description' => 'text',
            'row_config' => 'number',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['jdls_pricing_id', 'description', 'frequency']],
            [['create', 'update'], ['jdls_pricing_id', 'description', 'frequency', 'row_config']],
            ['delete', true],
        ];
    }

    public function getRates()
    {
        return $this->hasMany(PricingRate::class, ['jdls_pricing_period_id' => 'id'])
            ->where(['is_special' => false]);
    }

    public function getSpecialRates()
    {
        return $this->hasMany(PricingRate::class, ['jdls_pricing_period_id' => 'id'])
            ->where(['is_special' => true]);
    }

    public function getOriginalPricingId()
    {
        return $this->getOldAttribute('jdls_pricing_id');
    }

    public function getPricing()
    {
        return $this->hasOne(Pricing::class, ['id' => 'originalPricingId']);
    }

}