<?php

namespace app\modules\pricing\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\ngrest\plugins\SelectRelationActiveQuery;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\member\models\Member;
use app\modules\pricing\models\PricingRate;

use yii\behaviors\TimestampBehavior;

/**
 * Invoice.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property integer $jdls_member_id
 * @property integer $rate_id
 * @property string $reference
 * @property date $start_date
 * @property date $end_date
 * @property double $amount
 * @property integer $status
 * @property string $pdf_link
 */
class Invoice extends NgRestModel
{

    use SoftDeleteTrait;

     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_invoice';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-pricing-invoice';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jdls_member_id' => Yii::t('app', 'Membre'),
            'rate_id' => Yii::t('app', 'Abonnement'),
            'membership_rate' => Yii::t('app', 'Abonnement'),
            'reference' => Yii::t('app', 'Référence'),
            'formatted_reference' => Yii::t('app', 'Référence'),
            'start_date' => Yii::t('app', 'Début'),
            'end_date' => Yii::t('app', 'Fin'),
            'amount' => Yii::t('app', 'Montant'),
            'status' => Yii::t('app', 'Statut'),
            'pdf_link' => Yii::t('app', 'Pdf Link'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jdls_member_id', 'rate_id'], 'required'],
            [['jdls_member_id', 'rate_id', 'status', 'is_special', 'has_subscription_fee', 'start_date', 'end_date'], 'integer'],
            [['amount'], 'number'],
            [['reference', 'pdf_link'], 'string', 'max' => 255],
            [['jdls_member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['jdls_member_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['reference', 'start_date', 'end_date', 'pdf_link'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'jdls_member_id' => [
                'class' => SelectRelationActiveQuery::class, 
                'query' => $this->getMember(), 
                'labelField' => ['first_name', 'last_name']
            ],
            'rate_id' => [
                'class' => SelectRelationActiveQuery::class, 
                'query' => $this->getRate(), 
                'labelField' => 'frequency'
            ],
            'reference' => 'text',
            'amount' => 'decimal',
            'status' => ['toggleStatus', 'initValue' => 0],
            'pdf_link' => 'text',
            'start_date' => 'date',
            'end_date' => 'date',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngrestExtraAttributeTypes()
    {
        return [
            'formatted_reference' => 'text',
            'membership_rate' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['jdls_member_id', 'formatted_reference', 'start_date', 'end_date', 'amount', 'status']],
            [['create', 'update'], ['jdls_member_id', 'rate_id', 'reference', 'start_date', 'end_date', 'amount', 'status']],
            ['delete', true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return ['formatted_reference', 'membership_rate'];
    }

    public function getMembership_rate() 
    {}

    public function getFormatted_reference() 
    {
        return $this->is_special ? $this->reference . ' | Cross-Training' : $this->reference ;
    }

    public function getRate() 
    {
        return $this->hasOne(PricingRate::class, ['id' => 'rate_id']);
    }

    public function getMember() 
    {
        return $this->hasOne(Member::class, ['id' => 'jdls_member_id'])
            ->with('jdls_pricing_period_id');
    }

    public function getStatusText() 
    {
        $status = [
            0 => 'En attente',
            1 => 'Payé',
        ];
        return $status[$this->status];
    }
}