<?php

namespace app\modules\pricing\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use app\modules\pricing\models\PricingPeriod;

use yii\behaviors\TimestampBehavior;

/**
 * Pricing Rate.
 * 
 * File has been created with `crud/create` command. 
 *
 * @property integer $id
 * @property integer $jdls_pricing_period_id
 * @property text $description
 * @property integer $frequency
 * @property double $amount
 */
class PricingRate extends NgRestModel
{

    use SoftDeleteTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();       
        return $behaviors;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_pricing_rate';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-pricing-pricingrate';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jdls_pricing_period_id' => Yii::t('app', 'Périod'),
            'description' => Yii::t('app', 'Description'),
            'frequency' => Yii::t('app', 'Fréquence'),
            'amount' => Yii::t('app', 'Montant'),
            'is_special' => Yii::t('app', 'Tarif spécial')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jdls_pricing_period_id'], 'required'],
            [['jdls_pricing_period_id', 'frequency', 'is_special'], 'integer'],
            [['description'], 'string'],
            [['amount'], 'number'],
            [['jdls_pricing_period_id'], 'exist', 'skipOnError' => true, 'targetClass' => PricingPeriod::className(), 'targetAttribute' => ['jdls_pricing_period_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['description'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'jdls_pricing_period_id' => [
                'selectModel',
                'modelClass' => PricingPeriod::className(),
                'valueField' => 'id',
                'labelField' => function($model) { return $model->pricing->category . ' - ' . $model->description; }
            ],
            'description' => 'text',
            'frequency' => 'number',
            'amount' => 'decimal',
            'is_special' => ['toggleStatus', 'initValue' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['jdls_pricing_period_id', 'description', 'frequency', 'is_special', 'amount']],
            [['create', 'update'], ['jdls_pricing_period_id', 'description', 'frequency', 'is_special', 'amount']],
            ['delete', true],
        ];
    }

    public function getPeriod()
    {
        return $this->hasOne(PricingPeriod::class, ['id' => 'originalPeriodId']);
    }


    public function getOriginalPeriodId()
    {
        return $this->getOldAttribute('jdls_pricing_period_id');
    }

}