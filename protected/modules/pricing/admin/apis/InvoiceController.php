<?php

namespace app\modules\pricing\admin\apis;

/**
 * Invoice Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class InvoiceController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\pricing\models\Invoice';
}