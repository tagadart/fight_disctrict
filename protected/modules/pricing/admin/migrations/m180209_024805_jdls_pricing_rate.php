<?php

use yii\db\Migration;

/**
 * Class m180209_024805_jdls_pricing_rate
 */
class m180209_024805_jdls_pricing_rate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_pricing_rate', [
            'id' => $this->primaryKey(),
            'jdls_pricing_period_id' => $this->integer()->notNull(),
            'description' => $this->text(),
            'frequency' => $this->integer(),
            'amout' => $this->double()
        ]);

        $this->addForeignKey(
            'fk-jdls_pricing_rate-jdls_pricing_period_id',
            'jdls_pricing_rate',
            'jdls_pricing_period_id',
            'jdls_pricing_period',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-jdls_pricing_rate-jdls_pricing_period_id',
            'jdls_pricing_rate'
        );

        $this->dropTable('jdls_pricing_rate');
    }

}
