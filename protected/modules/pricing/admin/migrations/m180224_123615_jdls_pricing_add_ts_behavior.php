<?php

use yii\db\Migration;

/**
 * Class m180224_123615_jdls_pricing_add_ts_behavior
 */
class m180224_123615_jdls_pricing_add_ts_behavior extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jdls_invoice', 'created_at', $this->integer());
        $this->addColumn('jdls_invoice', 'updated_at', $this->integer());

        $this->addColumn('jdls_pricing', 'created_at', $this->integer());
        $this->addColumn('jdls_pricing', 'updated_at', $this->integer());

        $this->addColumn('jdls_pricing_period', 'created_at', $this->integer());
        $this->addColumn('jdls_pricing_period', 'updated_at', $this->integer());

        $this->addColumn('jdls_pricing_rate', 'created_at', $this->integer());
        $this->addColumn('jdls_pricing_rate', 'updated_at', $this->integer());

        /* Schedule */
        $this->addColumn('jdls_schedule', 'created_at', $this->integer());
        $this->addColumn('jdls_schedule', 'updated_at', $this->integer());

        /* Settings */
        $this->addColumn('jdls_settings', 'created_at', $this->integer());
        $this->addColumn('jdls_settings', 'updated_at', $this->integer());

        /* Team Member */
        $this->addColumn('jdls_team_member', 'created_at', $this->integer());
        $this->addColumn('jdls_team_member', 'updated_at', $this->integer());

        /* Training */
        $this->addColumn('jdls_training', 'created_at', $this->integer());
        $this->addColumn('jdls_training', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_invoice', 'created_at');
        $this->dropColumn('jdls_invoice', 'updated_at');

        $this->dropColumn('jdls_pricing', 'created_at');
        $this->dropColumn('jdls_pricing', 'updated_at');

        $this->dropColumn('jdls_pricing_period', 'created_at');
        $this->dropColumn('jdls_pricing_period', 'updated_at');

        $this->dropColumn('jdls_pricing_rate', 'created_at');
        $this->dropColumn('jdls_pricing_rate', 'updated_at');

        /* Schedule */
        $this->dropColumn('jdls_schedule', 'created_at');
        $this->dropColumn('jdls_schedule', 'updated_at');
        
        /* Settings */
        $this->dropColumn('jdls_settings', 'created_at');
        $this->dropColumn('jdls_settings', 'updated_at');
        
        /* Team Member */
        $this->dropColumn('jdls_team_member', 'created_at');
        $this->dropColumn('jdls_team_member', 'updated_at');
        
        /* Training */
        $this->dropColumn('jdls_training', 'created_at');
        $this->dropColumn('jdls_training', 'updated_at');
    }

}
