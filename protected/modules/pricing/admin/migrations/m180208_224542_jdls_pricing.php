<?php

use yii\db\Migration;

/**
 * Class m180208_224542_jdls_pricing
 */
class m180208_224542_jdls_pricing extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_pricing', [
            'id' => $this->primaryKey(),
            'category' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'amount' => $this->double()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('jdls_pricing');
    }

}
