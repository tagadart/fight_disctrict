<?php

use yii\db\Migration;

/**
 * Class m180208_230622_jdls_pricing
 */
class m180208_230622_jdls_pricing extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('jdls_pricing', 'currency', $this->string()->notNull());
        $this->addColumn('jdls_pricing', 'duration', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_pricing', 'currency');
        $this->dropColumn('jdls_pricing', 'duration');
    }

}
