<?php

use yii\db\Migration;

/**
 * Class m180209_005016_jdls_pricing_period
 */
class m180209_005016_jdls_pricing_period extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_pricing_period', [
            'id' => $this->primaryKey(),
            'jdls_pricing_id' => $this->integer()->notNull(),
            'frequency' => $this->integer(),
            'description' => $this->text(),
            'row_config' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-jdls_pricing_period-jdls_pricing_id',
            'jdls_pricing_period',
            'jdls_pricing_id',
            'jdls_pricing',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-jdls_pricing_period-jdls_pricing_id',
            'jdls_pricing_period'
        );

        $this->dropTable('jdls_pricing_period');
    }

}
