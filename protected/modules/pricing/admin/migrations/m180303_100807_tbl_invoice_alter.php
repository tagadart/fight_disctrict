<?php

use yii\db\Migration;

/**
 * Class m180303_100807_tbl_invoice_alter
 */
class m180303_100807_tbl_invoice_alter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('jdls_invoice', 'start_date', 'integer');
        $this->alterColumn('jdls_invoice', 'end_date', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('jdls_invoice', 'start_date', 'integer');
        $this->alterColumn('jdls_invoice', 'end_date', 'integer');
    }

}
