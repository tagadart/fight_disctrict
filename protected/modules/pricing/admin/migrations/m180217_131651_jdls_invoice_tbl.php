<?php

use yii\db\Migration;

/**
 * Class m180217_131651_jdls_invoice_tbl
 */
class m180217_131651_jdls_invoice_tbl extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('jdls_invoice', [
            'id' => $this->primaryKey(),
            'jdls_member_id' => $this->integer()->notNull(),
            'rate_id' => $this->integer(),
            'reference' => $this->string(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'amout' => $this->double(),
            'status' => $this->integer(),
            'pdf_link' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-jdls_invoice-jdls_member_id',
            'jdls_invoice',
            'jdls_member_id',
            'jdls_member_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('jdls_invoice');

        $this->dropForeignKey(
            'fk-jdls_invoice-jdls_member_id',
            'jdls_invoice'
        );
    }

}
