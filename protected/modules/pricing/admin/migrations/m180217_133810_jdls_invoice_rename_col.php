<?php

use yii\db\Migration;

/**
 * Class m180217_133810_jdls_invoice_rename_col
 */
class m180217_133810_jdls_invoice_rename_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('jdls_invoice', 'amout', 'amount');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->dropColumn('jdls_invoice', 'amout');
    }

}
