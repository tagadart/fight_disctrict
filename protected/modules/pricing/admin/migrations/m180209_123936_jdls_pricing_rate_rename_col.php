<?php

use yii\db\Migration;

/**
 * Class m180209_123936_jdls_pricing_rate_rename_col
 */
class m180209_123936_jdls_pricing_rate_rename_col extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('jdls_pricing_rate', 'amout', 'amount');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
         $this->dropColumn('jdls_pricing_rate', 'amout');
    }

}
