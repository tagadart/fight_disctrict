<?php

namespace app\modules\pricing\admin\controllers;

/**
 * Pricing Period Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class PricingPeriodController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\pricing\models\PricingPeriod';
}