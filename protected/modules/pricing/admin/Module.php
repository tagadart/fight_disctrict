<?php

namespace app\modules\pricing\admin;

/**
 * Pricing Admin Module.
 *
 * File has been created with `module/create` command. 
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
    	'api-pricing-pricing' => 'app\modules\pricing\admin\apis\PricingController',
    	'api-pricing-pricingperiod' => 'app\modules\pricing\admin\apis\PricingPeriodController',
    	'api-pricing-pricingrate' => 'app\modules\pricing\admin\apis\PricingRateController'
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Tarifs', 'monetization_on')
		        ->group('Group')
			        ->itemApi('Types d\'abonnements', 'pricingadmin/pricing/index', 'label', 'api-pricing-pricing')
			        ->itemApi('Périodicités', 'pricingadmin/pricing-period/index', 'label', 'api-pricing-pricingperiod')
			        ->itemApi('Tarifications/Périodes', 'pricingadmin/pricing-rate/index', 'label', 'api-pricing-pricingrate');
	}
}