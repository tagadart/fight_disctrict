<?php

namespace app\modules\teammember\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\traits\SoftDeleteTrait;

use yii\behaviors\TimestampBehavior;

/**
 * Team Member.
 * 
 * File has been created with `crud/create` command on LUYA version 1.0.0-RC4. 
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $function
 * @property text $description
 * @property integer $image
 * @property string $email
 * @property string $facebook_link
 * @property string $instagram_link
 * @property string $youtube_link
 */
class TeamMember extends NgRestModel
{

    use SoftDeleteTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TimestampBehavior::className();
        return $behaviors;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jdls_team_member';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-teammember-teammember';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'Prénom'),
            'last_name' => Yii::t('app', 'Nom'),
            'function' => Yii::t('app', 'Fonction'),
            'training' => Yii::t('app', 'Cours'),
            'prize_list' => Yii::t('app', 'Palmarès'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'email' => Yii::t('app', 'Email'),
            'facebook_link' => Yii::t('app', 'Lien Facebook'),
            'instagram_link' => Yii::t('app', 'Lien Instagram'),
            'youtube_link' => Yii::t('app', 'Lien Youtube'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'description', 'image'], 'required'],
            [['description'], 'string'],
            [['image'], 'integer'],
            [['first_name', 'last_name', 'training', 'prize_list', 'function', 'email', 'facebook_link', 'instagram_link', 'youtube_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function genericSearchFields()
    {
        return ['first_name', 'last_name', 'function', 'description', 'email', 'facebook_link', 'instagram_link', 'youtube_link'];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'first_name' => 'text',
            'last_name' => 'text',
            'function' => 'text',
            'description' => 'textarea',
            'training' => 'textArray',
            'prize_list' => 'textArray',
            'image' => ['file', 'fileItem' => true],
            'email' => 'text',
            'facebook_link' => 'text',
            'instagram_link' => 'text',
            'youtube_link' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['first_name', 'last_name', 'function']],
            [['create', 'update'], ['first_name', 'last_name', 'function', 'description', 'training', 'prize_list', 'image', 'email', 'facebook_link', 'instagram_link', 'youtube_link']],
            ['delete', true],
        ];
    }
    
}
