<?php

namespace app\modules\teammember\admin\controllers;

/**
 * Team Member Controller.
 * 
 * File has been created with `crud/create` command on LUYA version 1.0.0-RC4. 
 */
class TeamMemberController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teammember\models\TeamMember';
}