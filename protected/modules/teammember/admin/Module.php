<?php

namespace app\modules\teammember\admin;

/**
 * Teammember Admin Module.
 *
 * File has been created with `module/create` command on LUYA version 1.0.0-RC4. 
 */
class Module extends \luya\admin\base\Module
{
	public $apis = [
    	'api-teammember-teammember' => 'app\modules\teammember\admin\apis\TeamMemberController',
	];

	public function getMenu()
	{
	    return (new \luya\admin\components\AdminMenuBuilder($this))
	        ->node('Team', 'contacts')
	        ->group('Group')
	        ->itemApi('Liste des coaches', 'teammemberadmin/team-member/index', 'label', 'api-teammember-teammember');
	}
}