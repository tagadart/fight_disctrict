<?php

use yii\db\Migration;

/**
 * Class m180221_130913_jdls_teammember_add_cols
 */
class m180221_130913_jdls_teammember_add_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jdls_team_member', 'training', $this->string());
        $this->addColumn('jdls_team_member', 'prize_list', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jdls_team_member', 'training');
        $this->dropColumn('jdls_team_member', 'prize_list');
    }

}
