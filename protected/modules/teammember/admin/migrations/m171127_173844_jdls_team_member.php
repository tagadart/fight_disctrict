<?php

use yii\db\Migration;

class m171127_173844_jdls_team_member extends Migration
{
    public function safeUp()
    {
        $this->createTable('jdls_team_member', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'function' => $this->string(),
            'description' => $this->text()->notNull(),
            'image' => $this->integer()->notNull(),
            'email' => $this->string(),
            'facebook_link' => $this->string(),
            'instagram_link' => $this->string(),
            'youtube_link' => $this->string()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('jdls_team_member');
    }
}
