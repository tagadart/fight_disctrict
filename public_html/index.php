<?php
// composer autoload include
require(__DIR__ . '/../protected/vendor/autoload.php');

// use the luya boot wrapping class
$boot = new \luya\Boot();
$boot->configFile = '../protected/configs/env.php';

$boot->setBaseYiiFile(__DIR__ . '/../protected/vendor/yiisoft/yii2/Yii.php');
$boot->run();
